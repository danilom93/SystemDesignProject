# Raspberry Pi 3 model B setup
### Requirements
- Micro SD card (at least 8 GB)
- USB power supply

### Image download
Official images for recommended operating systems are available to download from the Raspberry Pi website Downloads page. For example, Raspbian image can be downloaded from [Download page](https://www.raspberrypi.org/downloads/raspbian/). Push Download ZIP on **RASPBIAN STRETCH WITH DESKTOP**.

### Flashing the SD card
To flash the SD card, you need a PC and can use *Etcher tool* that can be downloaded from  [Download page](https://etcher.io), then install it. 
- Extract the downloaded zip file containing the image (.img) file of Raspbian OS and open Etcher tool.
- Insert the SD card to the PC
- Select the image file in Etcher and check that the SD card is correctly detected
- Push the Flash button (it will takes a while)

After having flashed the Micro SD card, you can insert it. The Raspbian OS starts automatically when Raspberry is connected to the power supply.

### Enable SSH connection
To make the Raspberry Pi manageable with a remote connection for further configurations, you need to enable the SSH connection.
First of all, connect it to a keyboard, mouse and monitor using a HDMI cable and to the network using an ethernet cable. Then, perform the following operations:
- Open a terminal window from Raspberry Pi
- Launch commands:

```
$ sudo systemctl enable ssh
$ sudo systemctl start ssh
```

Now SSH is enabled.
To use the SSH connection, you need to know the IP address of the ethernet network of Raspberry Pi. Type the following command on a terminal window to display it:

```
$ hostname -I
```

### Set static IP
To set a static IP, you can use the already configured SSH or you can connect again to a monitor. For doing things faster, use the SSH connection. On a terminal window of your PC type:

- **ssh pi@ip_address** to connect remotely to the Raspberry Pi
- Write *yes* if it is needed (usually for the first time only)
- Insert password (default = *raspberry*)

Now you are connected.
Launch the command **sudo /etc/dhcpcd.conf**. Find the following lines, usually placed at the end of the file:
```
interface wlan0
static ip_address=192.168.1.201
static routers=192.168.1.254
static domain_name_servers=8.8.8.8
static domain_search=
```
Since you connected the Raspberry Pi through an ethernet connection, replace **wlan0** with **eth0**.
Of course, you have to change *static ip_address=xxx.xxx.xxx.xxx* if you want or leave it as is.
Finally, type **sudo reboot** to restart your device and apply changes.

### PyModBus installation
Now you need to install PyModBus on the Raspberry Pi.
Via SSH connection, type the following commands:

```
$ git clone git://github.com/bashwork/pymodbus.git
$ cd pymodbus
$ sudo python setup.py install
```
You will find a *pymodbus* folder in your home folder.

Or you can type only:
```
$ sudo apt-get install python-pymodbus
```

to install just the pymodbus library.
After that, PyModBus is correctly installed.

### Running PyModBus examples
You can run examples that are available in folder */examples/common/* to check the behaviour.
For example, run *synchronous server* and *client* examples. Launch them in the following order using:

```
$ python synchronous_server.py
$ python synchronous_client.py
```

Transactions happen between client and server and appear on the screen.




