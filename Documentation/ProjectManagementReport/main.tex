\documentclass[12pt, a4paper]{article}
\usepackage[title]{appendix}

\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour},   
	commentstyle=\color{codegreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{codegray},
	stringstyle=\color{codepurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}

\lstset{style=mystyle}
\usepackage{python}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[caption=false]{subfig}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{adjustbox}
\usepackage{siunitx}
\usepackage{sidecap}
\usepackage{caption}
\usepackage{float}
\usepackage{hyperref}

\usepackage{multirow}
\usepackage{multicol}
\usepackage{blindtext}
\usepackage{wrapfig}
\textwidth 16.5cm
\textheight 23cm
\topmargin -1cm
\oddsidemargin -0.5cm
\linespread{1.1}

\setcounter{secnumdepth}{5}

\usepackage[paper=portrait,pagesize]{typearea}
\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\setlength\parindent{0pt}

\begin{document}

	\begin{titlepage}
		
		\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here
		
		\center % Center everything on the page
		 
		%----------------------------------------------------------------------------------------
		%	HEADING SECTIONS
		%----------------------------------------------------------------------------------------
		
		\textsc{\LARGE Politecnico di Torino}\\[1.5cm] % Name of your university/college
		\textsc{\Large Computer Engineering - Embedded Systems}\\[0.5cm] % Major heading such as course name
		\textsc{\large System Design Project}\\[0.5cm] % Minor heading such as course title
		
		%----------------------------------------------------------------------------------------
		%	TITLE SECTION
		%----------------------------------------------------------------------------------------
		
		\HRule \\[0.4cm]
		{ \huge \bfseries WSS Project Management Report}\\[0.4cm] % Title of your document
		\HRule \\[1.5cm]
		 
		%----------------------------------------------------------------------------------------
		%	AUTHOR SECTION
		%----------------------------------------------------------------------------------------
		
		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \large
				\emph{Author:}\\
				Antonio Ciardo \\  % Your name
				Giovanni Antonio Me\\
				Piergiovanni Ferrara\\
				Danilo Moceri
			\end{flushleft}
		\end{minipage}
		~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \large
				\emph{Supervisor:} \\
				Paolo Prinetto  \\% Supervisor's Name
				Giuseppe Airò Farulla
			\end{flushright}
		\end{minipage}\\[2cm]
		
		% If you don't want a supervisor, uncomment the two lines below and remove the section above
		%\Large \emph{Author:}\\
		%John \textsc{Smith}\\[3cm] % Your name
		
		%----------------------------------------------------------------------------------------
		%	DATE SECTION
		%----------------------------------------------------------------------------------------
		
		{\large \today}\\[2cm] % Date, change the \today to a set date if you want to be precise
		
		%----------------------------------------------------------------------------------------
		%	LOGO SECTION
		%----------------------------------------------------------------------------------------
		
		\includegraphics[width=0.4\textwidth]{Images/poli.png} % Include a department/university logo - this will require the graphicx package
		 
		%----------------------------------------------------------------------------------------
		
		\vfill % Fill the rest of the page with whitespace
	
	\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}
To better follow the evolution of the project and to ensure that the original objectives will be maintained, it is always better to start a project:
\begin{itemize}
\item defining the requirements
\item defining the activities
\item scheduling the activities
\item deciding the methodologies to be used.
\end{itemize}

This will also help to satisfy the customer needs, saving time and effort for both.

\section{Requirements}
We received from our customers the request to make an emulator of a real water supply system, as much similar as possible, and to secure it. During the first phase of studying we also discussed with the customer to know if the proposed architecture met his requests, changing it accordingly.

\section{Tools}
To support us in the project planning and development we exploited different tools.
\begin{itemize}
\item \textbf{Redmine}: web-application for project management, we used it to define  features, tracking and plot the Gantt chart.
\item \textbf{Git}: one of the most used tools for tracking changes and coordinating the work on files among the team members. We used it also to define tasks and assign them to a couple of members, that could work independently.    
\item \textbf{Overleaf}: we used it to concurrently write the same document during documentations creation.
\end{itemize}

\section{Methodologies}
The picked methodology will have an ongoing impact on how the team will work and on the final outcome.

\subsection{Agile}
The principal methodology we followed during the whole project is Agile. This permits to be more reactive to the changes of customer requirements and to better adapt to the project evolution.
Agile is based on incremental and iterative developments: the work is divided in smaller ones, realizing at first a simplified version of the project and then adding and reviewing in each iteration new functionalities.
Each iteration, using \textit{SCRUM}, is known as \textit{Sprint}. In our project the most important iterations are three, as shown in the following picture:

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.7\linewidth]{Images/sprints}}
	\caption{Agile Sprints management}
	\label{fig:sprints}
\end{figure}

In each sprint we integrated or implemented a new feature of the project:
\begin{itemize}
\item \textbf{Sprint 1}: realization of the first communication and handling of sensors/actuators.
\item \textbf{Sprint 2}: implementation of the controlling application.
\item \textbf{Sprint 3}: integration of security.
\end{itemize}

\subsection{Pair Programming}
Generally, we assigned each defined task in Git to two members of the team: one is the driver that writes, the other one is the observer that reviews but then they exchange their roles. Each pair was chosen using a random selection.
We adopted this technique not only for writing code but also for the documentation.

\subsection{Git branching}
We used the \textit{Master} branch for maintaining a working and stable version of the system, while in the \textit{Development} branch we worked introducing new features that could compromise the system behavior. Once the \textit{Development} was bug-free, we merged these two branches into the \textit{Master}, then we created a new \textit{Release} branch from which customers could download the latest stable version.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.7\linewidth]{Images/git_branches}}
	\caption{Git branches}
	\label{fig:git_branches}
\end{figure}

For what concerns programming languages we mostly used, it is summarized with the following pie chart:

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/programming_lang}}
	\caption{Predominant programming languages}
	\label{fig:prog_lang}
\end{figure}

PHP, JavaScript, CSS and HTML languages have been used to develop the web page, while Python language has been exploited to implement controller and slaves algorithms.
Commits on Master and Development branches have been done on average every day. Total number of commits and average per day information are shown in the following picture for Master branch. More detailed charts are also provided that report the number of commits per day of a month and commits per weekday. From the latter, you may notice that we mostly worked on the project on weekends (Saturdays and Sundays), since in other days we were committed with thesis work. Git commits are just related to coding work because we also used Overleaf for tracking the documentations.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/master_commits}}
	\caption{Commits on Master branch}
	\label{fig:master_commits}
\end{figure}

The same for Development branch:

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/development_commits}}
	\caption{Commits on Development branch}
	\label{fig:dev_commits}
\end{figure}

\section{Activities Planning}
After having received the first requirements, we decided which were the most important phases of the project. They are: 

\begin{itemize}
  \item \textbf{State of art analysis}: study of real water supply systems, their architecture, vulnerabilities and communication protocols. 
  \item \textbf{System model design}: based on the knowledge achieved in the previous phase, decide the architecture of the WSS emulator and the protocols to be used.
  \item \textbf{Development}: waiting the necessary equipments from the customer, write software and integrate it in working prototypes. New functionalities are added and tested one by one.
  \item \textbf{Documentations}: final aim is to write the documents that will be used by both users and future developers, even though we started doing it earlier for tracking every working steps.
  \item \textbf{Release}: review and verification of the project before releasing it. The project is completed and there are only small changes to be done to improve stability and reliability. At the end of this phase the product will be ready for the customer.
\end{itemize}

The project significant events are labeled as \textit{Milestone} or \textit{Deliverable} (if they are released to the customer), which in this project are represented in the following table:

\begin{table}[H]
\centering
\label{my-label}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Phase}               & \textbf{Milestone}                                                                     & \textbf{Deliverable}                                                             \\ \hline
State of art analysis        & ---                                                                                    & ---                                                                              \\ \hline
System model design          & Architecture Definition                                                                & ---                                                                              \\ \hline
\multirow{3}{*}{Development} & \begin{tabular}[c]{@{}c@{}}First prototype with\\ controlling application\end{tabular} & ---                                                                              \\ \cline{2-3} 
                             & \begin{tabular}[c]{@{}c@{}}Integrated\\ Human Machine Interface\end{tabular}           & ---                                                                              \\ \cline{2-3} 
                             & Secured HMI                                                                            & ---                                                                              \\ \hline
Documentations               & ---                                                                                    & \begin{tabular}[c]{@{}c@{}}Release of User and Developer \\ manuals\end{tabular} \\ \hline
Release                      & ---                                                                                    & System release                                                                   \\ \hline
\end{tabular}
\end{table}

Cause we adopted an Agile development the different phases can overlap between them, which is opposed to the waterfall method in which phases are done is sequence. In the following figure is reported the \textit{Gantt} chart, where is possible to observe what previously said.


\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/Gantt}}
	\caption{Gantt chart}
	\label{fig:gantt}
\end{figure}

\section{Conclusion}
Using the described methodologies and planning strategies, we reached our goal of realizing the water supply system emulator, meeting both the requests of the customer and timing.


\end{document}
