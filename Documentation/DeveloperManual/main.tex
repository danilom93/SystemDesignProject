\documentclass[12pt, a4paper]{article}
\usepackage[title]{appendix}

\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour},   
	commentstyle=\color{codegreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{codegray},
	stringstyle=\color{codepurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}

\lstset{style=mystyle}
\usepackage{python}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[caption=false]{subfig}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{adjustbox}
\usepackage{siunitx}
\usepackage{sidecap}
\usepackage{caption}
\usepackage{float}
\usepackage{hyperref}

\usepackage{multirow}
\usepackage{multicol}
\usepackage{blindtext}
\usepackage{wrapfig}
\textwidth 16.5cm
\textheight 23cm
\topmargin -1cm
\oddsidemargin -0.5cm
\linespread{1.1}

\setcounter{secnumdepth}{5}

\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\setlength\parindent{0pt}

\begin{document}

	\begin{titlepage}
		
		\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here
		
		\center % Center everything on the page
		 
		%----------------------------------------------------------------------------------------
		%	HEADING SECTIONS
		%----------------------------------------------------------------------------------------
		
		\textsc{\LARGE Politecnico di Torino}\\[1.5cm] % Name of your university/college
		\textsc{\Large Computer Engineering - Embedded Systems}\\[0.5cm] % Major heading such as course name
		\textsc{\large System Design Project}\\[0.5cm] % Minor heading such as course title
		
		%----------------------------------------------------------------------------------------
		%	TITLE SECTION
		%----------------------------------------------------------------------------------------
		
		\HRule \\[0.4cm]
		{ \huge \bfseries WSS Developer Manual}\\[0.4cm] % Title of your document
		\HRule \\[1.5cm]
		 
		%----------------------------------------------------------------------------------------
		%	AUTHOR SECTION
		%----------------------------------------------------------------------------------------
		
		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \large
				\emph{Author:}\\
				Antonio Ciardo \\  % Your name
				Giovanni Antonio Me\\
				Piergiovanni Ferrara\\
				Danilo Moceri
			\end{flushleft}
		\end{minipage}
		~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \large
				\emph{Supervisor:} \\
				Paolo Prinetto  \\% Supervisor's Name
				Giuseppe Airò Farulla
			\end{flushright}
		\end{minipage}\\[2cm]
		
		% If you don't want a supervisor, uncomment the two lines below and remove the section above
		%\Large \emph{Author:}\\
		%John \textsc{Smith}\\[3cm] % Your name
		
		%----------------------------------------------------------------------------------------
		%	DATE SECTION
		%----------------------------------------------------------------------------------------
		
		{\large \today}\\[2cm] % Date, change the \today to a set date if you want to be precise
		
		%----------------------------------------------------------------------------------------
		%	LOGO SECTION
		%----------------------------------------------------------------------------------------
		
		\includegraphics[width=0.4\textwidth]{Images/poli.png} % Include a department/university logo - this will require the graphicx package
		 
		%----------------------------------------------------------------------------------------
		
		\vfill % Fill the rest of the page with whitespace
	
	\end{titlepage}

\tableofcontents
\newpage

\section{WSS emulator}

\subsection*{The ideal emulator}
The proposed water supply system, WSS for short, tries to mimic a real system as much as possible in a scaled version.
The main elements are:

\begin{itemize}
    \item Tanks
    \begin{itemize}
        \item Tank1
        \item Tank2
    \end{itemize}
    \item Actuators
    \begin{itemize}
        \item Pump
        \item Electro-valve
    \end{itemize}
    \item Sensors
    \begin{itemize}
        \item Environmental Humidity and Temperature
        \item Waterproof temperature
        \item Water flow
        \item Ultrasonic for distance measurement 
    \end{itemize}
\end{itemize}

In the following, the broad picture is shown:

\begin{figure}[h]
	\centering
	\fbox{\includegraphics[width=0.7\linewidth]{Images/WSS}}
	\caption{Ideal WSS model}
	\label{fig:wss}
\end{figure}

Tank1 is the reservoir, therefore the source of water, while the Tank2 contains the purified water.
\paragraph*{How does the water flow?} 
\mbox{} \newline
Water accumulated in Tank1 is pumped towards Tank2 but it can get back to the first in order to simulate the user usage through an electro-valve.\\

%\newpage

\subsection{Implemented emulator}

To avoid expensive sensors and actuators, it is also possible building up the entire system with commons electronic components, as we did for our emulator.
Specifically, the image \ref{fig:system_model} shows the chosen implementation.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/system_model}}
	\caption{WSS system model}
	\label{fig:system_model}
\end{figure}

The used materials are:
\begin{itemize}

\item Raspberry Pi master

\item Raspberry Pi Slave 1:
\begin{itemize}
    \item LED1 replaces pump actuator
    \item Environmental Humidity and Temperature sensor
\end{itemize}

\item Raspberry Pi Slave 2:
\begin{itemize}
    \item Ultrasonic sensor for distance measurement
    \item LED2 replaces electro-valve actuator
    \item Button1 replaces the purification process
    \item Button2 simulates the user request of water
\end{itemize}

\item 3-Ethernet ports Switch 
\item 2 SEcube USB stick
\end{itemize}

\noindent
For the sake of simplicity,  from now on, we will refer to the master as \textbf{RPi M} and slaves as \textbf{RPi S1} and as \textbf{RPi S2} respectively .

\section{Communication protocols}

\subsection{Modbus}

Since our emulator is implemented in a modular way, the various nodes composing the emulator need to communicate each other. 

Many are the possible choices to allow the communication among industrial devices, but the proper one had to be selected.

As already shown in the figure \ref{fig:system_model}, the adopted communication protocol between Raspberry Pis is ModBus TCP/IP over Ethernet. Therefore, in order to link Master and Slaves we need a switch to route the messages.

\paragraph*{Why ModBus?}
\mbox{} \newline
We chose ModBus because it is the ‘de-facto’ communication protocol in the industrial environment for connecting electronic devices.
Modbus enables communication among devices connected to the same network and it is often used to connect a supervisory computer with data acquisition systems.
A Modbus slave exposes to the Modbus master a given number of registers, which are:
\begin{table}[H]
\centering
\label{modbusObjects}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Object type} & \textbf{Access} & \textbf{Size} \\ \hline
Coil                 & Read/Write      & 1 bit         \\ \hline
Discrete input       & Read only       & 1 bit         \\ \hline
Input register       & Read only       & 16 bits       \\ \hline
Holding register     & Read/Write      & 16 bits       \\ \hline
\end{tabular}
\caption{Modbus objects}
\end{table}

\paragraph*{Why ModBus TCP/IP?}
\mbox{} \newline
Besides Modbus TCP/IP, there is another variant of Mudbus itself, called Modbus RTU 
\begin{itemize}
    \item ModBus TCP/IP is used for communications over TCP/IP networks
    \item ModBus RTU is used in serial communication
\end{itemize}
ModBus TCP/IP results to be a better choice because Raspberry Pi provides an Ethernet interface (TCP/IP stack is supported). Furthermore, TCP/IP offers retransmission (in presence of error), traffic congestion control and packet routing (packets go only through the interested nodes, while RTU uses broadcast messages).

Anyway, the used python library, named \textit{pymodbus}, allows to easily turn the entire system into a one communicating with Modbus RTU. This modification should be possible by simply connecting the Modbus RTU converters to the raspberry Pis and changing a  few lines of code.

More in details, it should be enough commenting and uncommenting in each Modbus device file the line of code reported below:

\begin{lstlisting}[language=Python]

#from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.client.sync import ModbusSerialClient as ModbusClient

#client = ModbusClient('localhost', port=5020)
client = ModbusClient(method='rtu', port='/dev/ptyp0', timeout=1, baudrate=9600)

\end{lstlisting}

\subsection{Sensors/Actuators - Raspberry Communication}

Slaves Raspberry Pis have to communicate with sensors and actuators to retrieve information and control the environment. Although in some cases it could be really easy dealing with sensors (such in the case of a LED) and actuators, there are other cases that require more complex protocols.

In this section, we will describe how Raspberry Pi slaves are connected to sensors and actuators and how the communication in case of more complex protocols happens.\\

\textbf{RPi S1} uses the following electronic components:
\begin{itemize}
    \item DHT11 (temperature and humidity sensor)
    \item Red LED
    \item Resistances: one of 560$\Omega$ and one of 10k$\Omega$
\end{itemize}
\mbox{}\newline
RPi S1 communicates with the DHT11 sensor using GPIO peripheral. This sensor has four pins: it needs one pin to retrieve the data from the sensor, two pins for power supply, while one is unused. DHT11 basically consists of a NTC temperature sensor or thermistor and a humidity sensor component. For what concerns the actuator, one GPIO pin is used to control the LED.

\newpage

\paragraph*{How does DHT11 communicate with RPi?} 
\mbox{} \newline
It follows a precise data transfer protocol with the microprocessor of the RPi that uses GPIO. The CPU configures the GPIO pin as output to send the \textit{Start} signal to the sensor, then the same GPIO pin  becomes an input to get the sensor response. \\ Once the start phase is completed, the sensor sends 40 bits (5 bytes) of data (MSB first).

\begin{table}[H]
  \centering
  \label{loginAttempts}
\begin{tabular}{|c|c|c|c|c|}
\hline
\textbf{Humidity High} & \textbf{Humidity Low} & \textbf{Temperature High} & \textbf{Temperature Low} & \textbf{Checksum} \\ \hline
byte 1 & byte 2 & byte 3 & byte 4 & byte 5 \\ \hline
\end{tabular}
\end{table}

The first and second bytes store the humidity value (\%), the third and fourth store the temperature value (°C), the last is used as checksum byte to make sure that the data transfer has completed without any error. \\To signal a 1 or 0, the sensor properly pulls and keeps high the data line for a certain amount of time according to the bit it has to send (for bit value 1, data line stays high for more time).\\
Below is reported a timing diagram that can help to understand the basics of the one wire communication protocol.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/onewire.png}}
	\caption{One wire protocol}
	\label{fig:onewire}
\end{figure}

\newpage
The picture below shows how to properly connect the electronic components attached to the RPi S1.
\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.65\linewidth]{Images/RPi_S1.png}}
	\caption{RPi S1}
	\label{fig:RPi S1}
\end{figure}

\noindent
\textbf{RPi S2} uses the following electronic components:
\begin{itemize}
    \item HC-SR04 Module (ultrasonic sensor)
    \item Red LED
    \item Two Buttons
    \item Resistors: three of 1k$\Omega$ and one of 560$\Omega$
    \item Jumper wires
    \item Breadboard
\end{itemize}

RPi S2 uses four GPIO pins (\textit{TRIG}, \textit{ECHO}, VCC and GND) to communicate with the ultrasonic sensor, another GPIO pin to control the LED and other two to check whether the buttons have been pressed or not.

\paragraph*{How does HC-SR04 module work?}
\mbox{} \newline
HC-SR04 Module is able to generate a 40kHz ultrasound wave that can travel through the air and get back to the module when an obstacle is encountered. 

By knowing the speed of the sound and the time it takes from the emission and the comeback, the distance from the obstacle is computed.

In order to generate the ultrasound wave you need to set and keep high the TRIG pin for 10 µs and then an 8 cycle sonic burst is generated. The ECHO pin will output the time the wave takes to travel forward and backward, that has to be multiplied by the sound speed to retrieve the distance. 
Below is reported a picture that shows how the HC-SR04 module, connected to a raspberry pi, works:
\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.8\linewidth]{Images/sr04.jpg}}
	\caption{HC-SR04 Module}
	\label{fig:sr04}
\end{figure}

Thus, in order to know the obstacle distance, we need to divide the multiplication result by 2.
To sum up, the following formula can be used to compute the distance:

\begin{equation}
	Distance[cm] = High\:level\:time \cdot \frac{Speed\:of\:sound}{2} = High\:level\:time \cdot \frac{34300}{2}
\end{equation}

The picture below shows how to properly connect the electronic components attached to the RPi S2.
\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.65\linewidth]{Images/RPi_S2.png}}
	\caption{RPi S2}
	\label{fig:RPi S2}
\end{figure}

\section{Software implementation}

\label{sw_implementation}

In this section we will characterize the software implementation of the emulator components.
Specifically, we will present an overview of the software architecture, followed by detailed descriptions for each and every device composing the system.

\subsection{Introduction}

Control behavior (in RPi M) is modeled as a finite state machine and implemented using \textit{Python}. RPi slaves software implementation is based on the same language.
Before launching the programs on them, there are some parameters that can be configured by specifying them in \textit{.ini} files for RPi M, RPi S1 and RPi S2. Programs are able to read these configuration files just at the beginning in the constructor function.\\\\
Configurations include:
\begin{itemize}
	\item RPi Slaves IP Address
	\item SQL DB parameters
	\item Controller Update Rate
	\item Temperature Update Rate
	\item Water Level Update Rate
	\item ModBus Devices Address and Pins
\end{itemize}

\subsection{RPi M}

RPi M monitors and controls the overall system, acting as a client, therefore it periodically connects to RPi slaves. Hereinafter, its behavior along with the configuration and implementation details are described. 

\subsubsection{Controller overview}

RPi M periodically asks for the water level (emulated with the distance read by the ultrasonic sensor) to the RPi S2. 
There are two possible cases:

\begin{itemize}
    \item \textbf{water level $<$ fullLevelThreshold:} Tank is not full, therefore, the controller orders to the slave S1 to turn on the water pump (emulated by a LED1) until the tank is filled.
\end{itemize}

\begin{itemize}
    \item \textbf{water level $>$ fullLevelThreshold:} Tank is full, therefore, the controller orders to the slave S1 to turn off the water pump (emulated by a LED1).
\end{itemize}

Supposing of having the tank2 filled, the system waits until the purification process is completed (emulated by pushing the Button1).
Then, if there is a user request of water (by pushing the Button2), the master activates the electro-valve connected to S2 (emulated by a LED2) that empties the Tank2 as long as the water request is stopped (by pushing again the Button). 

After stopping the distribution process, if the water level is lower than the \textit{refillThreshold} means  there is not enough water, so it is needed to re-establish its full quantity by turning on the pump. 
But, of course, then the water has to be purified again. 

The flow chart below shows the control mechanism of the system to clarify its behavior.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{Images/control_flow}
	\caption{Control flow}
	\label{fig:control_flow}
\end{figure}

\subsubsection{RPi M advanced configurations}
\label{master_configurations}

As already anticipated, it is possible to configure some parameters without modifying the source code. Indeed, configuration files are provided for tuning common settings. 

For RPi M, the configuration file is located in \textit{/home/pi/GitLab/SystemDesignProject/Sources/Master} and named \textit{master\_conf.ini}. It looks like the following:

\begin{lstlisting}[basicstyle=\tiny]

[Default]
pumpSlaveIPAddress: 192.168.0.201  ;	the IP address of the RPi S1
levelSlaveIPAddress: 192.168.0.202 ;	the IP address of the RPi S2

usernameDB: user                   ;	the username of the Database
passwordDB: password               ;	the password of the Database
hostDB: localhost                  ;	the host of the Database
nameDB: valuesSensorsActuators     ;	the name of the Database

controllerUpdateRate: 0.1          ;	the update rate of the controller flow
temperatureUpdateRate: 60          ;	the update rate of the temperature sample

levelHRAddress: 0x10               ;	ModBus holding register of level sensor
requestWaterHRAddress: 0x11        ;	ModBus holding register of request water
purificationDoneHRAddress: 0x12    ;	ModBus holding register of purification
valveHRAddress: 0x13               ;	ModBus holding register of valve

pumpHRAddress: 0x10                ;	ModBus holding register of pump
temperatureHRAddress: 0x11         ;	ModBus holding register of temperature sensor
humidityHRAddress: 0x12            ;	ModBus holding register of humidity sensor

maxHistoryTemperature: 100         ;	Max history size of temperature measurements
maxHistoryLevel: 100               ;	Max history size of level measurements
maxHistoryValve: 100               ;	Max history size of valve states
maxHistoryPump: 100                ;	Max history size of pump states

fullLevelThreshold: 30             ;	Full tank threshold
refillLevelThreshold: 80           ;	Refill tank threshold

debugPrints: 1                     ;	Flag for debug prints

\end{lstlisting}

The developer can modify this configuration file according to his needs. 

The one shown above is the [\textit{Default}] configuration. It is possible to add new configurations by adding [\textit{YourConfigName}] and the desired parameters, in the same file.

For example, you can change the SQL database parameters (\textit{usernameDB}, \textit{passwordDB}, \textit{hostDB} and \textit{nameDB}). For what concerns RPi slaves IP addresses, you can change \textit{pumpSlaveIPAddress} and \textit{levelSlaveIPAddress} parameters. 

Also, ModBus registers for sensors/actuators connected to RPi slaves, can be chosen as wanted.

Furthermore, as RPi M owns an SQL database (see section \ref{sql_database}), you can decide how much history information, for temperature and level sensors, will be stored on that, in terms of number of rows in tables (use \textit{maxHistoryTemperature} and \textit{maxHistoryLevel}).

One important parameter is the update rate (\textit{controllerUpdateRate} and \textit{temperatureUpdateRate}); its meaning will be explained in sub-section \ref{controller_implementation}.

\textbf{Note: Configuration name can be specified as input when launching the program using the proper command.}

\subsubsection{Controller implementation details}
\label{controller_implementation}

Controller is implemented by using a Python class, that provides a means of bundling data and functionality together.

The \textit{\_\_init\_\_} method represents a constructor that is called when instancing an object of the class in the main function. Inside constructor, parameters are read from the configuration file (see sub-section \ref{master_configurations}) and set.
Its prototype is:

\begin{lstlisting}[language=Python]
def __init__(self, confFileName, confName):
\end{lstlisting}

\mbox{}\newline
There are basically two threads that run with a different rate. 

Their rates are taken by the configuration file along with all the other tunable parameters as already described above.

Threads are instanced inside \textit{run} function that is:

\begin{lstlisting}[language=Python]
def run(self):

  loop_temp = LoopingCall(f=self.__getTemperatureHumidity)
  loop_temp.start(self.__temperatureUpdateRate, now=False)

  loop_temp = LoopingCall(f=self.__controllerThread)
  loop_temp.start(self.__controllerUpdateRate, now=False)

  reactor.run()
\end{lstlisting}

\mbox{}\newline
The two threads perform the following operations:

\begin{itemize}
\item \textbf{getTemperatureHumidity}: connects to RPi S1 and gets temperature/humidity measurements. Then, it updates the information on the database. The update rate is \textit{temperatureUpdateRate}.
\item \textbf{controllerThread}: connects to RPi S2 and gets the water level measurement by calling \textit{getLevel} function, then its value is inserted into the database (\textit{insertLevel} function). The update rate is \textit{controllerUpdateRate}. After that, according to the measured level, it sends the turn ON/OFF command to RPi S1 for the Pump (\textit{setPump} function), so, the FSM states can evolve. If the water level is low, the pump remains active until the \textit{fullLevelThreshold} is reached.
When purification is done (\textit{purificationIsDone} function call returns \textit{0}), i.e. the button has been pressed, the controller goes into states where the water request can be started/ended by pushing the other button. For emulating the water distribution, the valve state is set accordingly (\textit{setValve} function). Note that after the water has been purified, the request can be satisfied many times until the \textit{refillLevelThreshold} is reached, that means when the water level is too low.
\end{itemize}

To avoid race conditions, critical sections are protected by mutex. The main race conditions are created when:
\begin{itemize}
\item Connecting to a slave for reading and writing registers
\item Accessing the mysql database
\end{itemize}
A mutex is acquired before entering a critical section and released after exiting from that.

\subsection{RPi S1}

Slave 1 exposes objects to the Modbus master acting as a server, therefore receives data requests from the system Controller. 

An overview on it and configurations are described hereinafter.

\subsubsection{Slave 1 overview}

RPi S1 owns the temperature/humidity sensor and the water Pump (emulated by LED1). 

It exposes to the RPi M temperature and humidity measurements and receives from it the command for turning ON/OFF the Pump.

\subsubsection{Slave 1 advanced configurations}
\label{slave1_configurations}

As in the case of the master, it is possible to configure some application parameters without editing the source code.

For RPi S1, the configuration file is located in \textit{home/pi/GitLab/SystemDesignProject/Sources/Slave\_pump\_temp} and named \textit{slave\_pump\_conf.ini}. It looks like the following:

\begin{lstlisting}[basicstyle=\tiny]
[Default]
pumpPin: 11                ;	the pump pin to which the Raspberry Pi is connected
DHTPin: 4                  ;	the DHT pin to which the Raspberry Pi is connected

IPAddress: 192.168.1.201   ;	the IP address of the RPi S1 itself

temperatureUpdateRate: 5   ;	the update rate of the temperature sample 
pumpUpdateRate: 0.01       ;	the update rate of the pump status

pumpHRAddress: 0x10        ;	ModBus holding register of the pump
temperatureHRAddress: 0x11 ;	ModBus holding register of temperature sensor
humidityHRAddress: 0x12    ;	ModBus holding register of humidity sensor

debugPrints: 0             ;	Flag for debug prints
\end{lstlisting}

The developer can modify this configuration file according to his needs. 

The one shown above, as for the RPi M, is the [\textit{Default}] configuration, but others can be added in the same file. 

Its server IP address is already set as default (\textit{IPAddress} parameter) to \textit{192.168.0.203} but it can be easily changed. 

Similar modifications are possible also for ModBus registers that expose the sensors and actuators data.

Other important parameters are the update rates (\textit{temperatureUpdateRate} and \textit{pumpUpdateRate}). Their meaning will be explained in sub-section \ref{slave1_implementation}.

\subsubsection{Slave 1 implementation details}
\label{slave1_implementation}

Slave 1 is implemented by using a Python class.\\
Also here, the \textit{\_\_init\_\_} method is called when instancing an object of the class in the main function.
Inside constructor, parameters are read from the configuration file (see sub-section \ref{slave1_configurations}) and set.\\
Its prototype is:

\begin{lstlisting}[language=Python]
def __init__(self, confFileName, confName):
\end{lstlisting}

Basically, the slave 1 application uses two threads running at given rates (settable with the configuration files) and performing precise operations.
They are instanced inside \textit{runServer} function:

\begin{lstlisting}[language=Python]
def runServer(self):

  store = ModbusSlaveContext(
    di=ModbusSequentialDataBlock(0, [17]*100),
    co=ModbusSequentialDataBlock(0, [17]*100),
    hr=ModbusSequentialDataBlock(0, [0]*100),
    ir=ModbusSequentialDataBlock(0, [17]*100))
    
  self.context = ModbusServerContext(slaves=store, single=True)
  loop_pump = LoopingCall(f=self.pumpControllerThread)
  loop_pump.start(self.pumpUpdateRate, now=False)

  loop_temp = LoopingCall(f=self.temperatureHumidityThread)
  loop_temp.start(self.temperatureUpdateRate, now=False)

  identity = ModbusDeviceIdentification()
  identity.VendorName = self.vendorName
  identity.ProductCode = self.productCode
  identity.VendorUrl = self.vendorUrl
  identity.ProductName = self.productName
  identity.ModelName = self.modelName
  identity.MajorMinorRevision = self.majorMinorRevision

  print("Starting...")
      StartTcpServer(self.context, identity=identity, address=(self.IPAddress, 5020))
\end{lstlisting}

The Slave 1 exposes registers that can be read and write by the Controller. 

They are defined in the first few lines of the runServer function and are much more than the needed ones. This could allow a future network expansion in terms of number of sensors/actuators.

Threads perform the following operations:

\begin{itemize}
	\item \textbf{pumpControllerThread}: read the given Modbus register to switch ON/OFF the pump accordingly with the register content (by calling \textit{pumpON} and \textit{pumpOFF} functions). The update rate of this operation is \textit{pumpUpdateRate}.
	\item \textbf{temperatureHumidityThread}: reads the temperature/humidity from the DHT11 sensor and stores the measurements to the corresponding Modbus registers. The update rate of this operation is \textit{temperatureUpdateRate}.
\end{itemize}

Mutex are used to avoid race conditions, specifically, they are acquired before entering a critical section and released after exiting it.

\subsection{RPi S2}

Also slave 2 acts as a Modbus server, therefore it receives data requests from the Controller.

An overview on it and configurations are described hereinafter.

\subsubsection{Slave 2 overview}

RPi S2 owns the ultrasonic sensor (for water level measurements), a Valve (emulated by LED2) and two buttons. 

It sends to the RPi M water level measurements when requested and receives from it the command for turning ON/OFF the Valve.

Buttons are used to inform about purification completion and starting/ending of water distribution, therefore buttons status are sent to the controller when it asks for them.

\subsubsection{Slave 2 advanced configurations}
\label{slave2_configurations}

Also in this case it is possible to set some application parameters without modifying the source code. 
Specifically, for RPi S2, the configuration file is located in \textit{home/pi/GitLab/SystemDesignProject/Sources/Slave\_level\_valve} and named \textit{slave\_level\_valve\_conf.ini}. It looks like the following:

\begin{lstlisting}[basicstyle=\tiny]
[Default]
valvePin: 17                     ;	the pump pin to which the Raspberry Pi is connected
purificationDonePin: 27          ;	the purif. pin to which the Raspberry Pi is connected
requestWaterPin: 18              ;	the request pin to which the Raspberry Pi is connected
triggerPin: 23                   ;	the trigger pin to which the Raspberry Pi is connected
echoPin: 24                      ;	the echo pin to which the Raspberry Pi is connected

IPAddress: 192.168.0.202         ;	the IP address of the RPi S1 itself

levelUpdateRate: 0.1             ;	the update rate of the level sensor
valveUpdateRate: 0.1             ;	the update rate of the valve

levelHRAddress: 0x10             ;	ModBus holding register of the level sensor
requestWaterHRAddress: 0x11      ;	ModBus holding register of the request 
purificationDoneHRAddress: 0x12  ;	ModBus holding register of the purification   
valveHRAddress: 0x13             ;	ModBus holding register of the valve

debugPrints: 1
\end{lstlisting}

The developer can modify this configuration file according to his needs. 
The one shown above, as for the RPi S1, is the [\textit{Default}] configuration, but others can be added in the same file and can be chosen when the application is run. 
Its server IP address is already set as default (\textit{IPAddress} parameter) to \textit{192.168.0.202} but it can be easily changed. Same for the ModBus registers.
Other important parameters are the update rates (\textit{levelUpdateRate} and \textit{valveUpdateRate}). Their meaning will be explained in sub-section \ref{slave2_implementation}.

\subsubsection{Slave 2 implementation details}
\label{slave2_implementation}

Slave 2 is implemented by using a Python class.\\
Also here, the \textit{\_\_init\_\_} method is called when instancing an object of the class in the main function. Inside constructor, parameters are read from the configuration file (see sub-section \ref{slave2_configurations}) and set.\\
Its prototype is:

\begin{lstlisting}[language=Python]
def __init__(self, confFileName, confName):
\end{lstlisting}
Basically, the slave 2 application uses two threads running at given rates (settable with the configuration files) and performing precise operations.
They are instanced inside \textit{runServer} function:

\begin{lstlisting}[language=Python]
def runServer(self):

        store = ModbusSlaveContext(
    		di=ModbusSequentialDataBlock(0, [17]*100),
    		co=ModbusSequentialDataBlock(0, [17]*100),
    		hr=ModbusSequentialDataBlock(0, [0]*100),
    		ir=ModbusSequentialDataBlock(0, [17]*100))
    	self.context = ModbusServerContext(slaves=store, single=True)

    	loop_level = LoopingCall(f=self.waterLevelThread)
        loop_level.start(self.levelUpdateRate, now=False)
    	loop_valve = LoopingCall(f=self.valveButtonsThread)
    	loop_valve.start(self.valveUpdateRate, now=False)

        identity = ModbusDeviceIdentification()
        identity.VendorName = self.vendorName
        identity.ProductCode = self.productCode
        identity.VendorUrl = self.vendorUrl
        identity.ProductName = self.productName
        identity.ModelName = self.modelName
        identity.MajorMinorRevision = self.majorMinorRevision

        print("Starting...")
        StartTcpServer(self.context, identity=identity, address=(self.IPAddress, 5020))

\end{lstlisting}

The Slave 2 exposes registers that can be read and write by the Controller. 
They are defined in the first few lines of the runServer function and are much more than the needed ones. 
This could allow a future network expansion in terms of number of sensors/actuators.
Threads perform the following operations:

\begin{itemize}
	\item \textbf{waterLevelThread}: reads the distance from the HC-SR04 sensor and stores the values to the corresponding Modbus register. The update rate of this operation is \textit{levelUpdateRate}.
	\item \textbf{valveButtonsThread}: reads the corresponding Modbus register to switch ON/OFF the valve (by driving GPIO outputs) and stores the Button1 (purification) and Button2 (water request) status to the respective registers. The update rate of this operations is \textit{valveUpdateRate}.
\end{itemize}

Mutex are used to avoid race conditions, specifically, they are acquired before entering a critical section and released after exiting it.

\subsection{Human Machine Interface (HMI)}
\label{HMI}
System users can monitor the entire system from a remote position through a web interface. This web interface is provided by an Apache server installed in the \textbf{RPi M} along with an SQL database. 

\subsubsection{Web Site}
\label{web_site}

\textit{Apache server} has been set on RPi M to provide a web site based on HTML, PHP and Javascript. 
The web interface offers a login mechanism, specifically, two levels of privilege exist: user and supervisor; 
the first allows to show states and measurements, whereas the second allows to also check the history based on time (by downloading \textit{.csv} files) of those measurements, along with charts.

The data stored in the database is shown in a web interface that changes according to the privilege level, after performing the login.
Provided information are related to Pump, Valve states and Temperature/Humidity, Water level measurements. The two different web interfaces are shown below.\\

The supervisor is provided with a real-time and more complete view on the system:
\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.95\linewidth]{Images/web_interface_supervisor_new}}
	\caption{Supervisor Web Interface}
	\label{fig:web_supervisor}
\end{figure}

While the unprivileged user can only track real-time information, without history data:
\begin{figure}[H]
	\centering
    \fbox{\includegraphics[width=0.8\linewidth]{Images/web_interface_user}}
	\caption{User Web Interface}
	\label{fig:web_user}
\end{figure}

More in details, the main content of the page is defined inside the \textit{data.php} file. The latter uses other php files to establish the connection with the database and to perform queries. Since a continuous refresh was needed, an Ajax mechanism with Javascript has been implemented. Thanks to this functionality it has been possible to provide an user interface with the capability of updating dynamically its content in reaction to the latest stimuli perceived by the sensors.  


\subsubsection{SQL Database}
\label{sql_database}
The system has to store data related to users of the system, sensors and actuators. 
For this reason, the Master RPi has been provided with a mysql database.
Specifically, the databases created are two, one for storing data related to the system users and the other to manage the information related to sensors, actuators and more in general the whole control system.\\

The SQL database receives connection both from the control application and from the web interface.
More in details, the two databases take the name of \textit{secure\_login} and \textit{valueSensorActuators}, below are reported the tables they contain. 
\paragraph*{secure\_login database} 
\mbox{}
\begin{table}[H]
  \centering
  \label{members}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{ID} & \textbf{Username} & \textbf{Email} & \textbf{Password} & \textbf{Salt} & \textbf{Role} \\ \hline
1   & admin         & admin@wss.com       & hash(128)          &  hash(128)   & 1     \\ \hline
---   & ---         & ---       & ---          &   ---   & ---     \\ \hline
\end{tabular}
\caption{Members table}
\end{table}

\begin{table}[H]
  \centering
  \label{loginAttempts}
\begin{tabular}{|c|c|}
\hline
\textbf{User ID} & \textbf{Time} \\ \hline
1  & 2018-06-02 23:19:12.3265         \\ \hline
---  & ---         \\ \hline
\end{tabular}
\caption{Login attempts table}
\end{table}

\paragraph*{valuesSensorsActuators database}
\mbox{}
\begin{table}[H]
  \centering
  \label{temp_hum}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Temperature} & \textbf{Humidity} & \textbf{Time} \\ \hline
23   & 44         & 2018-06-02 23:11:12.3695        \\ \hline
---   & ---         & ---        \\ \hline
\end{tabular}
\caption{Temperature Humidity table}
\end{table}

\begin{table}[H]
  \centering
  \label{water_levels}
\begin{tabular}{|c|c|}
\hline
\textbf{Level} & \textbf{Time} \\ \hline
45   & 2018-06-02 23:09:18.3995           \\ \hline
---   & ---           \\ \hline
\end{tabular}
\caption{Water levels table}
\end{table}

\begin{table}[H]
  \centering
  \label{pump}
\begin{tabular}{|c|c|}
\hline
\textbf{State} & \textbf{Time} 	\\ \hline
1   & 2018-06-02 24:01:11.8155         			\\ \hline
---   & ---         			\\ \hline
\end{tabular}
\caption{Pump table}
\end{table}

\begin{table}[H]
  \centering
  \label{valve}
\begin{tabular}{|c|c|}
\hline
\textbf{State} & \textbf{Time} \\ \hline
0   & 2018-06-02 24:02:01.5205           \\ \hline
---   & ---         			\\ \hline
\end{tabular}
\caption{Valve table}
\end{table}



\section{Security}
In order to secure the communication between RPi M and the Web interface two levels of cryptography are used. First, the HTTP connection is made secure by using an SSL certificate (see section \ref{web_site}), then it is further protected through \textit{SEcube} platform on client and server side.

\subsection{HTTP over SSL} 
The HTTP protocol is encrypted using the Secure Sockets Layer (SSL). An SSL connection between client and server is based on handshaking in order to: 
\begin{itemize}
	\item make sure that the client is talking to the right server (vice versa is optional).
    \item establish the encryption algorithm that will be used.
    \item agree on the keys necessary for this algorithm.
\end{itemize}
After having established the connection, client and server can communicate safely using the agreed algorithm and keys.

\subsection{SElink}
SElink is a dedicated security layer that leverages on the SEcube services to protect any client-server HTTP or HTTPS-based network application. This layer runs on top of any transport protocol. When a client needs to establish a secure communication to reach a server, the security library negotiates the session keys and performs the needed encryption, signature, decryption, and verification operations on the exchanged data. In our system the client is represented by the User-PC, whereas the server is the RPi M.\\
They are configured in this way:

\begin{itemize}
	\item \textbf{Client-side}: SElink software has to be installed on a 64-bit Windows 10 machine. It includes a driver, a background service and a Graphical User Interface (GUI) to set configurations.
	After installing it, the server IP address and communication ports range have to be set. Also, the full path to the source application's executable file that uses the secure service should be provided. Through the GUI, these parameters can be set.

	\item \textbf{Server-side}: SElink software can be installed on a Linux environment. In our system, it is put into the RPi M. On it, the port on which encrypted connections will be accepted and redirect host/port have to be configured. Server receives encrypted requests from the client on 8080 port, decrypts and redirects them to the configured host/port.
\end{itemize}






\end{document}
