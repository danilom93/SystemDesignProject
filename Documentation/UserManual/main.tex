\documentclass[12pt, a4paper]{article}

\usepackage[title]{appendix}
\usepackage{listings}

\usepackage[most]{tcolorbox}
\newtcblisting{commandshell}{colback=black,colupper=black,colframe=yellow!75!black,
	listing only,listing options={style=tcblatex,language=sh},
	every listing line={\textcolor{red}{\small\ttfamily\bfseries \$> }}}


\usepackage{color}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour},   
	commentstyle=\color{codegreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{codegray},
	stringstyle=\color{codepurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}

\lstset{style=mystyle}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[caption=false]{subfig}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{adjustbox}
\usepackage{siunitx}
\usepackage{sidecap}
\usepackage{caption}
\usepackage{float}
\usepackage{hyperref}

\usepackage{multirow}
\usepackage{multicol}
\usepackage{blindtext}
\usepackage{wrapfig}
\textwidth 16.5cm
\textheight 23cm
\topmargin -1cm
\oddsidemargin -0.5cm
\linespread{1.1}


\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\setlength\parindent{0pt}

\begin{document}

	\begin{titlepage}
		
		\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here
		
		\center % Center everything on the page
		 
		%----------------------------------------------------------------------------------------
		%	HEADING SECTIONS
		%----------------------------------------------------------------------------------------
		
		\textsc{\LARGE Politecnico di Torino}\\[1.5cm] % Name of your university/college
		\textsc{\Large Computer Engineering - Embedded Systems}\\[0.5cm] % Major heading such as course name
		\textsc{\large System Design Project}\\[0.5cm] % Minor heading such as course title
		
		%----------------------------------------------------------------------------------------
		%	TITLE SECTION
		%----------------------------------------------------------------------------------------
		
		\HRule \\[0.4cm]
		{ \huge \bfseries WSS User Manual}\\[0.4cm] % Title of your document
		\HRule \\[1.5cm]
		 
		%----------------------------------------------------------------------------------------
		%	AUTHOR SECTION
		%----------------------------------------------------------------------------------------
		
		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \large
				\emph{Author:}\\
				Antonio Ciardo \\  % Your name
				Giovanni Antonio Me\\
				Piergiovanni Ferrara\\
				Danilo Moceri
			\end{flushleft}
		\end{minipage}
		~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \large
				\emph{Supervisor:} \\
				Paolo Prinetto  \\% Supervisor's Name
				Giuseppe Airò Farulla
			\end{flushright}
		\end{minipage}\\[2cm]
		
		% If you don't want a supervisor, uncomment the two lines below and remove the section above
		%\Large \emph{Author:}\\
		%John \textsc{Smith}\\[3cm] % Your name
		
		%----------------------------------------------------------------------------------------
		%	DATE SECTION
		%----------------------------------------------------------------------------------------
		
		{\large \today}\\[2cm] % Date, change the \today to a set date if you want to be precise
		
		%----------------------------------------------------------------------------------------
		%	LOGO SECTION
		%----------------------------------------------------------------------------------------
		
		\includegraphics[width=0.4\textwidth]{Images/poli.png} % Include a department/university logo - this will require the graphicx package
		 
		%----------------------------------------------------------------------------------------
		
		\vfill % Fill the rest of the page with whitespace
	
	\end{titlepage}

\tableofcontents

\newpage

\section*{A real Water Supply System}

A real water supply system looks like the one in the following picture:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Images/real_model}
	\caption{A real water supply system model}
	\label{fig:real_model}
\end{figure}

\noindent
The main elements are:
\begin{itemize}
	\item \textbf{Source}: it is the reservoir that provides the raw water
	\item \textbf{Treatment plant}: where the water is treated before flowing towards the Storage Tank
	\item \textbf{Storage Tank}: tank that stores the purified water
	\item \textbf{Distribution Pipes}: water is provided to the catchment areas
\end{itemize}

\noindent
In figure \ref{fig:real_model} you can easily understand how the water flows by looking at the arrows.

Usually, real Water Supply Systems do not provide detailed information about their used architecture, especially for the part related to the control systems.

However, we have found many control systems producers that provide similar architectures for industrial applications. Thus, basing on that we developed our water supply system emulator.

\newpage

\section{WSS emulator}

\subsection*{The ideal emulator}

The proposed water supply system, WSS for short, tries to mimic a real system as much as possible in a scaled version.
The main elements are:

\begin{itemize}
    \item Tanks
    \begin{itemize}
        \item Tank1
        \item Tank2
    \end{itemize}
    \item Actuators
    \begin{itemize}
        \item Pump
        \item Electro-valve
    \end{itemize}
    \item Sensors
    \begin{itemize}
        \item Environmental Humidity and Temperature
        \item Waterproof temperature
        \item Water flow
        \item Ultrasonic for distance measurement 
    \end{itemize}
\end{itemize}

\noindent
In the following, the broad picture is shown:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Images/WSS}
	\caption{Ideal WSS model}
	\label{fig:wss}
\end{figure}

\noindent
Tank1 is the reservoir, therefore the source of water, while the Tank2 contains the purified water.
\paragraph{How does the water flow?}
\mbox{} \newline
Water accumulated in Tank1 is pumped towards Tank2 but it can get back to the first in order to simulate the consumer usage through an electro-valve.\\

\newpage

\subsection{Implemented emulator}
The presented emulator could be implemented in many ways, trading off the budget available and how much one wants stay close to a real water supply system.\\
To avoid expensive sensors and actuators, it is also possible building up the entire system with commons electronic components. 
This simplification allows to have a fully working emulator using just the material listed in section \ref{material}.\\
In this manual, the implemented WSS uses common and cheap electronic components to mimic real devices.\\
Specifically, the image \ref{fig:systemmodel} shows the chosen implementation.

Within the next sections the following arguments will be described:

\begin{itemize}
	\item How to setup the WSS system (section \ref{setup})
    \item How to mount the WSS system (section \ref{mounting})
	\item How to configure and run the WSS system (section \ref{configure})
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{Images/system_model}
	\caption{WSS system model}
	\label{fig:systemmodel}
\end{figure}

\newpage

\section{WSS required materials}

\label{material}

The needed materials for building up the WSS emulator using common electronic components are:

\begin{itemize}
	
	\item 3 Raspberry Pis
    \item 3 Power supplies 5V
    \item 3 USB cables (micro USB to USB)
	\item HC-SR04 sensor
	\item DHT11 sensor
	\item 2 LEDs
	\item 2 Buttons
	\item 2 Breadboards
	\item Jumper wires
	\item Resistances (560$\Omega$, 1k$\Omega$, 2k$\Omega$ and 10k$\Omega$)
	\item 3 Ethernet-ports Switch
	\item 3 Ethernet cables
	\item 1 Router
	\item 1 PC equipped with 64-bit Windows 10 (optionally with a linux-based OS too)
	\item 2 SEcube USB stick
    \item 3 micro SD card (at least 8 GB)
\end{itemize}

\newpage

\section{WSS setup}
\label{setup}

The user can mount the various system components as shown in figure \ref{fig:systemmodel}. 
Raspberry devices (\textbf{RPi M}, \textbf{RPi S1} and \textbf{RPi S2}) will have different sensors/actuators attached, their connection will be analyzed in section \ref{mounting}.

\subsection{General Rasberry Pis setup}

Here, the setup for each and every Raspberry Pi is provided.\\
In addiction to the Raspberry, the materials needed are:

\begin{itemize}
	\item Micro SD card (at least 8 GB)
	\item USB power supply
\end{itemize}

\subsubsection{Image download}

Official images for recommended operating systems are available to download from the Raspberry Pi website Downloads page. For example, Raspbian image can be downloaded from \href{https://www.raspberrypi.org/downloads/raspbian/}{Download page}. Push \textbf{Download ZIP} on \textbf{Raspbian Stretch with Desktop}.


\subsubsection{Flashing the SD card}

To flash the SD card, you need a PC and can use Etcher tool that can be downloaded from \href{https://etcher.io}{Download page}: 
\begin{enumerate}
	\item Extract the downloaded zip file containing the image (\textit{.img}) file of Raspbian OS and open Etcher tool.
	\item Insert the SD card to the PC
	\item Select the image file in Etcher and check that the SD card is correctly detected
	\item Push the Flash button (it will take a while)
\end{enumerate}

\noindent
After the insertion of the micro SD into the Raspberry Pi, the Raspbian OS starts automatically when Raspberry is connected to the power supply.

\subsubsection{Enable SSH connection}

To make the Raspberry Pi manageable with a remote connection for further configurations, you need to enable the SSH connection.
First of all, you have to connect the Raspberry to the keyboard, mouse and monitor using a HDMI cable and to the network using an ethernet cable. Then, you have to execute the following commands on a terminal window from Raspberry Pi:

\begin{lstlisting}
$	sudo systemctl enable ssh
$	sudo systemctl start ssh
\end{lstlisting}

\noindent
Now SSH is enabled.\\
To use the SSH connection, you need to know the IP address of the Ethernet network of Raspberry Pi. Type \textbf{hostname -I} on a terminal window to display it.

\subsubsection{Set static IP}
\label{static_ip}

To set a static IP, you can use the already configured SSH or you can connect the Raspberry to a monitor. For doing things faster, here SSH is used. On a linux shell of your PC type (on a Windows machine you can use Putty):

\begin{itemize}
	\item \textbf{ssh pi@$<$rasp\_ip\_address$>$} to connect remotely to the Raspberry Pi
	\item Write \textit{yes} if it is needed (usually for the first time only)
	\item Insert the password (\textit{default = raspberry}).
\end{itemize}

\noindent
Now you are connected.\\
Launch the command \textbf{sudo /etc/dhcpcd.conf}. Find the following lines, usually placed at the end of the file:

\begin{lstlisting}
interface eth0
static ip_address=192.168.1.201
static routers=192.168.1.254
static domain_name_servers=8.8.8.8
static domain_search=
\end{lstlisting}

\noindent
Of course, you have to change the static \textbf{ip\_address=xxx.xxx.xxx.xxx} with the desired one.\\ 
Finally, you should type:
\begin{lstlisting}
sudo reboot
\end{lstlisting}
to restart your device and apply the changes.

\subsubsection{PyModBus installation}

Now you need to install PyModBus on the Raspberry Pi.\\
Via SSH connection, type the following commands:

\begin{lstlisting}
$ git clone git://github.com/bashwork/pymodbus.git
$ cd pymodbus
$ sudo python setup.py install
\end{lstlisting}

\noindent
Or you can type only \textbf{sudo apt-get install python-pymodbus}
to install just the pymodbus library.\\

\subsubsection{Running PyModBus examples (optional)}
If you choose the first option, you will find a pymodbus folder in your home.
You can now run examples that are available in the folder \textbf{/examples/common/} to check whether PyModBus has been correctly installed.
For example, run synchronous server and client examples. First you should edit the source files (\textit{synchronous\_server.py} and \textit{synchronous\_client.py}) inserting the right IP addresses, then you can launch them in the following order using:

\begin{lstlisting}
$ python synchronous_server.py
$ python synchronous_client.py
\end{lstlisting}

\noindent
Transactions happen between client and server and appear on the screen. To interrupt the execution of client and server press \textbf{CTRL-C}.

\subsubsection{WSS software download}
You need to download the software, for WSS Controller and Slaves, from the GitLab repository. Linux distributions, like Raspbian, comes with Git software already installed.\\
Thus, you have to open SSH connections for each Raspberry and execute the following commands:

\begin{lstlisting}
$ cd ~
$ git clone https://gitlab.com/danilom93/SystemDesignProject.git
\end{lstlisting}

\noindent
Now you can see the \textbf{SystemDesignProject} folder inside \textit{home} folder where you can find all the needed files (software, configuration files and documentations/manuals).


\subsection{Raspberry Pi Master setup}
Raspberry Pi Master needs to be setup, differently from Raspberry Pi slaves, for managing the SQL database and the Web server.

\subsubsection{Install MySQL and DataBase initialization}

To install MySQl, type the following commands on a shell:

\begin{lstlisting}
$ sudo apt-get install mysql-server
$ sudo mysql_secure_installation
\end{lstlisting}

Now, MySQL should be correctly installed.\\
To create and initialize the needed database, type the following commands:

\begin{lstlisting}
$ cd SystemDesignProject/WebSiteAndScript/SQLScripts
$ sudo mysql -u root -p
\end{lstlisting}

Now insert the already set password, and from the mysql shell type:

\begin{lstlisting}
$ source script.sql
$ source scriptTables.sql
$ exit
\end{lstlisting}

Now, you should have the database created and ready to be used.

\subsubsection{Install Apache}

To install Apache web server, you should execute the following commands on a shell:
\begin{lstlisting}
$ sudo apt-get install apache2
$ sudo apt-get install phpmyadmin
\end{lstlisting}

\noindent
Edit the file \textbf{/etc/apache2/apache2.conf} and add the line \textbf{"Include /etc/phpmyadmin/apache.conf"}, then launch the command \textbf{sudo service apache2 restart}.\\

\subsubsection{SSL}
To install and configure SSL, type the following commands on a shell:
\begin{lstlisting}
$ sudo apt-get install openssl
$ sudo mkdir /etc/apache2/ssl
$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -out /etc/apache2/ssl/server.crt -keyout /etc/apache2/ssl/server.key 
$ sudo ln -s /etc/apache2/sites-available/default-ssl /etc/apache2/sites-enabled/000-default-ssl
\end{lstlisting}

\noindent
Edit the SSL configuration file with \textbf{sudo nano /etc/apache2/sites-enabled/000-default-ssl}.\\
Type \textbf{Ctrl+W} to search for \textit{SSLEngine} and make sure you find the line \textbf{SSLEngine on} and modify these file paths:

\begin{lstlisting}
SSLCertificateFile    /etc/apache2/ssl/server.crt
SSLCertificateKeyFile /etc/apache2/ssl/server.key
\end{lstlisting}

Then type the following commands:

\begin{lstlisting}
$ sudo a2enmod ssl
$ sudo a2ensite default-ssl
$ sudo service apache2 restart
\end{lstlisting}

\subsubsection{Redirect HTTP to HTTPS}

Now, you need to redirect all the HTTP requests to HTTPS.\\
In order to do that, you should execute the following commands:

\begin{lstlisting}
$ sudo a2enmod rewrite
$ sudo a2enmod ssl
\end{lstlisting}

Then, you have to edit the file \textbf{/etc/apache2/sites-available/000-default.conf} as shown below:

\begin{lstlisting}
<VirtualHost *:80>
RewriteEngine On
RewriteCond %{HTTPS} off
RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>

<VirtualHost *:443>
SSLEngine on
SSLCertificateFile <path to your crt file>
SSLCertificateKeyFile <path to your private key file>
# Rest of your site config
# ...
</VirtualHost>
\end{lstlisting}

\subsubsection{Web Site creation}
To provide the web server with the needed file, you should execute the following commands:
\begin{lstlisting}
$ cd ~
$ sudo cp -r SystemDesignProject/WebSiteAndScript/Website/var/www/html
\end{lstlisting}

Finally, restart the Apache server with:

\begin{lstlisting}
$ sudo service apache2 restart
\end{lstlisting}

\subsubsection{SElink installation}

In order to install SELink you should execute the following commands on a shell:

\begin{lstlisting}
$ cd ~
$ sudo apt-get install build-essential
$ sudo apt-get install libboost-all-dev
\end{lstlisting}

Last command can take a while. Then, type the following:

\begin{lstlisting}
$ wget https://www.secube.eu/download/SElinkSDK_Gen2017_10.zip
$ unzip SElinkSDK_Gen2017_10.zip
$ cd Data\ at\ Motion/proxy
$ make
$ sudo make install
$ cd ~
$ cd SystemDesignProject/WebsiteAndScript/SELink
$ sudo cp selinkgw.json /opt/selink/selinkgw.json
$ systemctl start selinkgw
\end{lstlisting}

Now, the daemon should be run and configured. If you want to stop it, type:

\begin{lstlisting}
$ systemctl stop selinkgw
\end{lstlisting}

\subsection{SElink Client installation}

To interact with the system you need a Windows PC from which you will access the system web interface. In this machine, you should install the required SElink software.\\ 
There are three components to be installed for the software to function properly:

\begin{itemize}
    \item SElink driver
    \item SElink service
    \item SElink GUI
\end{itemize}

You should download the software from
\href{https://www.secube.eu/download/SElinkSDK_Gen2017_10.zip}{Download page}, then unzip it.
Before installing, open an administrator command prompt and execute the following command:

\begin{lstlisting}
$ bcdedit /set TESTSIGNING ON
\end{lstlisting}

Now you need to restart the PC.

When you will not need anymore the software, you can execute:
\begin{lstlisting}
$ bcdedit /set TESTSIGNING OFF
\end{lstlisting}

\newpage

\section{WSS mounting}
\label{mounting}

Once you have setup all Raspberry Pis, you have to properly equip them with the other materials (see figure \ref{fig:systemmodel} and section \ref{setup}).
The following image, showing the raspberry's GPIOs, will be helpful for the user in the mounting phase.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Images/RPi_pins}
	\caption{}
	\label{fig:rpipins}
\end{figure}


\subsection{Raspberry Pi Master}
RPi M needs to be connected to the Switch with an \textit{Ethernet cable}. You should also attach the SEcube USB stick to secure the communication with the Web interface.


\subsection{Raspberry Pi Slave 1}
RPi S1 needed materials are:

\begin{itemize}
	\item DHT11
	\item LED1
	\item 1 Ethernet cable
	\item 1 BreadBoard
	\item 1 560$\Omega$ resistance
	\item 1 10k$\Omega$ resistance
	\item A bunch of wires
  	\item 1 Power supply 5V
    \item 1 USB cable (micro USB to USB)
\end{itemize}
To avoid damages, you should power off the raspberry. Then, you can connect the components as shown in the figure below:

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.8\linewidth]{Images/RPi_S1}}
	\caption{WSS Raspberry Pi S1}
	\label{fig:rpis1}
\end{figure}

\noindent
\textbf{DHT11} has 4 pins. From left to right, they are: \textit{VCC}, \textit{DATA}, \textit{NOT-CONNECTED} and \textit{GND}. Take the sensor and RPi as in figure and connect:

\begin{itemize}
	\item \textit{VCC}: to the red band of the bredboard
	\item \textit{DATA}: to PIN 7 of the RPi
	\item \textit{NOT-CONNECTED}: leave not connected
	\item \textit{GND}: to the blue band of the breadboard
\end{itemize}
Place 10k$\Omega$ resistor between DHT11 VCC and DATA\\

\noindent
\textbf{LED1} has only 2 pins, thus, connect the shorter terminal to the blue band and the other to PIN 11 of the RPi using a 560$\Omega$ resistor in the middle.\\
Finally, connect PIN 2 of the RPi, that is the 5V pin, to the red band of the breadboard, while PIN 6 of the RPi, that is GND, to the blue band of the breadboard.

\subsection{Raspberry Pi Slave 2}

RPi S2 needs materials that are:

\begin{itemize}
	\item HC-SR04
	\item LED2
	\item 2 Buttons
	\item 1 Ethernet cable
	\item 1 BreadBoard
	\item 1 560$\Omega$ resistance
	\item 3 1k$\Omega$ resistance
	\item A bunch of wires
\end{itemize}

To avoid damages, you should power off the raspberry. Then, you can connect the components as shown in the figure below:

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=0.8\linewidth]{Images/RPi_S2}}
	\caption{WSS Raspberry Pi S2}
	\label{fig:rpis2}
\end{figure}

\noindent
\textbf{HC-SR04} has 4 pins. From left to right, they are: \textit{VCC}, \textit{TRIGGER}, \textit{ECHO} and \textit{GND}. Take the sensor and RPi as in figure and connect:

\begin{itemize}
	\item \textit{VCC}: to the red band of the bredboard
	\item \textit{TRIGGER}: to PIN 16 of the RPi
	\item \textit{ECHO}: to PIN 18 of the RPi 
	\item \textit{GND}: to the blue band of the breadboard
\end{itemize}

\noindent
Furthermore, put other two 1k$\Omega$ between ECHO of the module and GND.\\
\textbf{LED1} has only 2 pins. Connect the LED shorter terminal to the blue band and the other to PIN 11 of the RPi using a 560$\Omega$ resistor in the middle\\
For \textbf{Button1}, you need to connect 2 pins. One to PIN 12 of the RPi and the other to the blue band of the breadboard.
For \textbf{Button2}, you need to connect 2 pins. One to PIN 13 of the RPi and the other to the blue band of the breadboard.\\
Finally, connect PIN 2 of the RPi, that is the 5V pin, to the red band of the breadboard, while PIN 6 of the RPi, that is GND, to the blue band of the breadboard.

\section{WSS configuration and run}
\label{configure}

This section will explain how to configure and run the already mounted system.

\subsection{Configure .ini files}

If you correctly followed the setup phase (see section \ref{setup}), the Rasberry PIs will have the software package inside \textbf{/home/pi/GitLab/SystemDesignProject} folder.\\
Each RPi device has a default configuration that reflects the wiring connections suggested in section \ref{mounting} and some other parameter such as the IP addresses.\\
The configuration files allow many customization, but basically just modifying the IP addresses should be enough to have whole system working properly.
In order to do that, you should move inside each device folder and edit the corresponding configuration file.
The content of these files is shown below.

\subsubsection{RPi M configuration file}

The configuration file is located in \textit{/home/pi/GitLab/SystemDesignProject/Sources/Master} and named \textit{master\_conf.ini}. It looks like:

\begin{lstlisting}[basicstyle=\tiny]

[Default]
pumpSlaveIPAddress: 192.168.0.201  ;	the IP address of the RPi S1
levelSlaveIPAddress: 192.168.0.202 ;	the IP address of the RPi S2

usernameDB: user                   ;	the username of the Database
passwordDB: password               ;	the password of the Database
hostDB: localhost                  ;	the host of the Database
nameDB: valuesSensorsActuators     ;	the name of the Database

controllerUpdateRate: 0.1          ;	the update rate of the controller flow
temperatureUpdateRate: 60          ;	the update rate of the temperature sample

levelHRAddress: 0x10               ;	ModBus holding register of level sensor
requestWaterHRAddress: 0x11        ;	ModBus holding register of request water
purificationDoneHRAddress: 0x12    ;	ModBus holding register of purification
valveHRAddress: 0x13               ;	ModBus holding register of valve

pumpHRAddress: 0x10                ;	ModBus holding register of pump
temperatureHRAddress: 0x11         ;	ModBus holding register of temperature sensor
humidityHRAddress: 0x12            ;	ModBus holding register of humidity sensor

maxHistoryTemperature: 100         ;	Max history size of temperature measurements
maxHistoryLevel: 100               ;	Max history size of level measurements
maxHistoryValve: 100               ;	Max history size of valve states
maxHistoryPump: 100                ;	Max history size of pump states

fullLevelThreshold: 30             ;	Full tank threshold
refillLevelThreshold: 80           ;	Refill tank threshold

debugPrints: 1                     ;	Flag for debug prints

\end{lstlisting}

\subsubsection{RPi S1 configuration file}

The configuration file is located in \textit{home/pi/GitLab/SystemDesignProject/Sources/Slave\_pump\_temp} and named \textit{slave\_pump\_conf.ini}. It looks like the following:

\begin{lstlisting}[basicstyle=\tiny]
[Default]
pumpPin: 11                ;	the pump pin to which the Raspberry Pi is connected
DHTPin: 4                  ;	the DHT pin to which the Raspberry Pi is connected

IPAddress: 192.168.1.201   ;	the IP address of the RPi S1 itself

temperatureUpdateRate: 5   ;	the update rate of the temperature sample 
pumpUpdateRate: 0.01       ;	the update rate of the pump status

pumpHRAddress: 0x10        ;	ModBus holding register of the pump
temperatureHRAddress: 0x11 ;	ModBus holding register of temperature sensor
humidityHRAddress: 0x12    ;	ModBus holding register of humidity sensor

debugPrints: 0             ;	Flag for debug prints
\end{lstlisting}


\subsubsection{RPi S2 configuration file}

The configuration file is located in \textit{home/pi/GitLab/SystemDesignProject/Sources/Slave\_level\_valve} and named \textit{master\_level\_valve\_conf.ini}. It looks like the following:

\begin{lstlisting}[basicstyle=\tiny]
[Default]
valvePin: 17                     ;	the pump pin to which the Raspberry Pi is connected
purificationDonePin: 27          ;	the purif. pin to which the Raspberry Pi is connected
requestWaterPin: 18              ;	the request pin to which the Raspberry Pi is connected
triggerPin: 23                   ;	the trigger pin to which the Raspberry Pi is connected
echoPin: 24                      ;	the echo pin to which the Raspberry Pi is connected

IPAddress: 192.168.0.202         ;	the IP address of the RPi S1 itself

levelUpdateRate: 0.1             ;	the update rate of the level sensor
valveUpdateRate: 0.1             ;	the update rate of the valve

levelHRAddress: 0x10             ;	ModBus holding register of the level sensor
requestWaterHRAddress: 0x11      ;	ModBus holding register of the request 
purificationDoneHRAddress: 0x12  ;	ModBus holding register of the purification   
valveHRAddress: 0x13             ;	ModBus holding register of the valve

debugPrints: 1
\end{lstlisting}


\subsection{Run WSS software programs}
Before powering up the boards, please check that all the connections have been correctly done, as explained in the previous phases. If yes, we are ready to run the Water Supply System.\\
Hereinafter we will explain how to run the programs on RPi S1, RPi S2 and RPi M.\\

\noindent
\textbf{Note}: follow the same order of execution of the programs

\subsubsection{RPi S1}
Connect to the RPi S1 via ssh (to do this see section \ref{static_ip}) by using its IP address and if connection is successful type one of the following commands:
\begin{itemize}
	\item launch slave with the default configuration
	\begin{lstlisting}
	$ python SystemDesignProject/Sources/Slave_pump_temp/main.py
	\end{lstlisting}
    
    \item launch slave with the desired configuration (e.g. \textit{ConfName})
	\begin{lstlisting}
	$ python SystemDesignProject/Sources/Slave_pump_temp/main.py ConfName
	\end{lstlisting}
\end{itemize}

\subsubsection{RPi S2}
Connect to the RPi S2 via ssh (to do this see section \ref{static_ip}) by using its IP address and if connection is successful, type the following command:

\begin{itemize}
	\item launch slave with the default configuration
	\begin{lstlisting}
	$ python SystemDesignProject/Sources/Slave_level_valve/main.py
	\end{lstlisting}
    
    \item launch slave with the desired configuration (e.g. \textit{ConfName})
	\begin{lstlisting}
	$ python SystemDesignProject/Sources/Slave_level_valve/main.py ConfName
	\end{lstlisting}
\end{itemize}


\subsubsection{RPi M}
Connect to the RPi M via ssh (to do this see section \ref{static_ip}) by using its IP address and if connection is successful, type the following command:

\begin{itemize}
	\item launch master with the default configuration
	\begin{lstlisting}
	$ python SystemDesignProject/Sources/Master/main.py
	\end{lstlisting}
    
    \item launch master with the desired configuration (e.g. \textit{ConfName})
	\begin{lstlisting}
	$ python SystemDesignProject/Sources/Master/main.py ConfName
	\end{lstlisting}
\end{itemize}
\newpage

\section{WSS user interface}

Previously a web server has been set on the RPi M to provide a web interface based on HTML, PHP and Javascript. 
We made it secure by installing an \textit{SSL} certificate and redirecting all server HTTP requests to \textit{HTTPS}. Moreover, we used SEcube and SElink to encrypt data in order to create a more reliable system.
The user interface provides useful informations related to the system status, such as: temperature, distance, humidity, pump and valve states.

Two levels of privilege exist: user and supervisor; the first allows to show states and measurements, whereas the second allows to also check the history based on time (by downloading \textit{.csv} files) of those measurements, along with charts.

\noindent
The user interfaces with the two different levels of privileges are shown below.\\

\begin{figure}[H]
	\centering
    \fbox{\includegraphics[width=0.85\linewidth]{Images/web_interface_user}}
	\caption{Unprivileged User Web Interface}
	\label{fig:web_user}
\end{figure}

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=1\linewidth]{Images/web_interface_supervisor_new}}
	\caption{Supervisor Web Interface}
	\label{fig:web_supervisor}
\end{figure}


\begin{appendices}
	
\end{appendices}

\end{document}
