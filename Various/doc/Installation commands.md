## INSTALL MYSQL

1. sudo apt-get install mysql-server
2. sudo mysql_secure_installation  
3. sudo mysql -u -p root

## INSTALL APACHE
1. sudo apt-get install apache2
2. sudo apt-get install phpmyadmin (it will show a GUI)
3. edit the file /etc/apache2/apache2.conf 
and add the line "Include /etc/phpmyadmin/apache.conf" 
4. sudo service apache2 restart

## [SSL](http://studyraspberrypi.blogspot.it/2015/12/setup-https-ssl-for-apache-server.html)
1. sudo apt-get install openssl
2. sudo mkdir /etc/apache2/ssl
3. sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -out /etc/apache2/ssl/server.crt -keyout /etc/apache2/ssl/server.key 
4. sudo ln -s /etc/apache2/sites-available/default-ssl /etc/apache2/sites-enabled/000-default-ssl
5. Edit the SSL configuration file: sudo nano /etc/apache2/sites-enabled/000-default-ssl
	Write: 
		SSLEngine on
		SSLCertificateFile    /etc/apache2/ssl/server.crt
		SSLCertificateKeyFile /etc/apache2/ssl/server.key
6. sudo a2enmod ssl
7. sudo a2ensite default-ssl
8. sudo service apache2 restart

## REDIRECT HTTP TO HTTPS:
1. sudo a2enmod rewrite
2. sudo a2enmod ssl
3. Edit the file: /etc/apache2/sites-available/000-default.conf 
    ```
		<VirtualHost *:80>
			RewriteEngine On
			RewriteCond %{HTTPS} off
			RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
		</VirtualHost>

		<VirtualHost *:443>
			SSLEngine on
			SSLCertificateFile <path to your crt file>
			SSLCertificateKeyFile <path to your private key file>
			# Rest of your site config
			# ...
		</VirtualHost>
	```
4. sudo service apache2 restart

		