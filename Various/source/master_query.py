
from pymodbus.client.sync import ModbusTcpClient
from pprint import pprint
import time
import mysql.connector

client = ModbusTcpClient('192.168.1.202', port=5020)
client.connect()
client1 = ModbusTcpClient('192.168.1.201', port=5020)
client1.connect()
cnx = mysql.connector.connect(user='user', password='password', host='127.0.0.1', database='prova')	
cursor = cnx.cursor()

while(1):

	rr = client.read_holding_registers(0x10, 1, unit=0x01) #addr, count, slave
	#print("Distance is: ")
	#print(rr.registers)
	data = (str(rr.registers[0]))
	add = "UPDATE slave_values set measurement=%s WHERE sensor='Distance' " % (data) 	
	#print(data)	
	cursor.execute(add)
	cnx.commit()
		
	if(int(rr.registers[0]) < 30):
		
		rq = client1.write_register(3, 1, unit=0x01)
	else:
		
		rq = client1.write_register(3, 0, unit=0x01)
	time.sleep(0.5)


cursor.close()
cnx.close()
client.close()
client1.close()
