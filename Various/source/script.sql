
DROP DATABASE if exists sql2234862;
CREATE DATABASE sql2234862;
USE sql2234862;

CREATE TABLE users (username varchar(30) key,
                    password varchar(40),
                    email varchar(100),
                    permission int);

CREATE TABLE temp_hum ( temperature int,
                        humidity int,
                        time timestamp key);

CREATE TABLE water_levels ( level int,
                            time timestamp key);

CREATE TABLE pump ( state boolean,
                    time timestamp key);

CREATE TABLE valve (  state boolean,
                      time timestamp key);

