from enum import Enum
from twisted.internet import reactor
from twisted.internet.task import LoopingCall
from pymodbus.client.sync import ModbusTcpClient

import time
import threading
import ConfigParser
import mysql.connector

class States(Enum):
	RESET = 1
	CHECK_DISTANCE = 2
	PURIFICATION_DONE = 3
	WATER_REQUEST = 4

class MasterController:

    def __init__(self, confFileName, confName):

        config = ConfigParser.ConfigParser()
        config.read(confFileName)

        self.__pumpSlaveIPAddress = config.get(confName, "pumpSlaveIPAddress")
        self.__levelSlaveIPAddress = config.get(confName, "levelSlaveIPAddress")
        self.__usernameDB = config.get(confName, "usernameDB")
        self.__passwordDB = config.get(confName, "passwordDB")
        self.__hostDB = config.get(confName, "hostDB")
        self.__nameDB = config.get(confName, "nameDB")
        self.__controllerUpdateRate = float(config.get(confName, "controllerUpdateRate"))
        self.__temperatureUpdateRate = float(config.get(confName, "temperatureUpdateRate"))
        self.__levelHRAddress = int(config.get(confName, "levelHRAddress"), 16)
        self.__requestWaterHRAddress = int(config.get(confName, "requestWaterHRAddress"), 16)
        self.__purificationDoneHRAddress = int(config.get(confName, "purificationDoneHRAddress"), 16)
        self.__valveHRAddress = int(config.get(confName, "valveHRAddress"), 16)
        self.__pumpHRAddress = int(config.get(confName, "pumpHRAddress"), 16)
        self.__temperatureHRAddress = int(config.get(confName, "temperatureHRAddress"), 16)
        self.__humidityHRAddress = int(config.get(confName, "humidityHRAddress"), 16)


        self.__maxHistoryTemperature = int(config.get(confName, "maxHistoryTemperature"), 10)
        self.__maxHistoryLevel = int(config.get(confName, "maxHistoryLevel"), 10)
        self.__maxHistoryValve = int(config.get(confName, "maxHistoryValve"), 10)
        self.__maxHistoryPump = int(config.get(confName, "maxHistoryPump"), 10)
        self.__fullLevelThreshold = int(config.get(confName, "fullLevelThreshold"), 10)
        self.__refillLevelThreshold = int(config.get(confName, "refillLevelThreshold"), 10)

        self.debugPrints = int(config.get(confName, "debugPrints"), 10)

        self.__mutexSlavePump = threading.RLock()
        self.__mutexSlaveLevel = threading.RLock()
        self.__mutexSQL = threading.RLock()

        self.__slavePump = ModbusTcpClient(self.__pumpSlaveIPAddress, port=5020)
        self.__slaveLevel = ModbusTcpClient(self.__levelSlaveIPAddress, port=5020)

        self.__cnx = mysql.connector.connect(   user=self.__usernameDB,
                                                password=self.__passwordDB,
                                                host=self.__hostDB,
                                                database=self.__nameDB)

        self.__cursor = self.__cnx.cursor()

        self.__state = States.RESET

        self.__pumpStateOld = 0
        self.__pumpState = 0

    def run(self):

        loop_temp = LoopingCall(f=self.__getTemperatureHumidity)
        loop_temp.start(self.__temperatureUpdateRate, now=False)

        loop_temp = LoopingCall(f=self.__controllerThread)
        loop_temp.start(self.__controllerUpdateRate, now=False)
        reactor.run()

    def __controllerThread(self):

	self.__insertLevel(str(self.__getLevel()))

        #FSM starts
        #Reset state
        if self.__state == States.RESET:

            self.__state = States.CHECK_DISTANCE

        #Check distance state
        elif self.__state == States.CHECK_DISTANCE:

            #If the level of water is lower than the set threshold
            if self.__getLevel() >= self.__fullLevelThreshold:

                self.__pumpState = 1
            else:

                self.__pumpState = 0

            #Checks if the pump state has to be switched
            if self.__pumpStateOld != self.__pumpState:

                #If the new state is different from the old one, drives the pump
                self.__setPump(self.__pumpState)
                self.__pumpStateOld = self.__pumpState

            #If the purification process has been completed
            if self.__purificationIsDone():

                self.__state = States.PURIFICATION_DONE

        #Purification done state
        elif self.__state == States.PURIFICATION_DONE:

            #If a water request arrived
            if self.__waterRequest():

                #Turns on the valve
                self.__setValve(1)
                self.__state = States.WATER_REQUEST

        #Water request state
        elif self.__state == States.WATER_REQUEST:

            #If a water stop request arrived
            if self.__waterRequest():

                #Turns off the valve
                self.__setValve(0)

                #Checks if a water refill process is needed
                if self.__getLevel() > self.__refillLevelThreshold:

                    #Water level is too low, restart from the beginning
                    self.__state = States.CHECK_DISTANCE
                else:

                    #Water level is enough to serve again a request, water is already purified
                    self.__state = States.PURIFICATION_DONE
        else:

            pass
    #Performs database queries
	self.__cnx.commit()

    def __getTemperatureHumidity(self):

        self.__mutexSlavePump.acquire()
        self.__slavePump.connect()
        rr = self.__slavePump.read_holding_registers(self.__temperatureHRAddress, 2, unit=0x01) #addr, count, slave
        temperature = str(rr.registers[0])
        humidity = str(rr.registers[1])
        self.__slavePump.close()

        self.__mutexSlavePump.release()
        self.__mutexSQL.acquire()

        query0 = "SELECT COUNT(*) FROM temp_hum"
        self.__cursor.execute(query0)

        tempCounter = self.__cursor.fetchone()
        if int(tempCounter[0]) > self.__maxHistoryTemperature :

			query0 = "DELETE FROM temp_hum WHERE time = (select * from (SELECT MIN(time) FROM temp_hum) t)"
			self.__cursor.execute(query0)

        query0 = "INSERT INTO temp_hum (temperature, humidity) VALUES (%s, %s)" % (temperature, humidity)
        self.__cursor.execute(query0)

        self.__mutexSQL.release()

    def __getLevel(self):

        self.__mutexSlaveLevel.acquire()
        self.__slaveLevel.connect()
        rr = self.__slaveLevel.read_holding_registers(self.__levelHRAddress, 1, unit=0x01) #addr, count, slave
        level = int(rr.registers[0])
        self.__slaveLevel.close()
        self.__mutexSlaveLevel.release()
        return level

    def __purificationIsDone(self):

        self.__mutexSlaveLevel.acquire()
        self.__slaveLevel.connect()
        rr = self.__slaveLevel.read_holding_registers(self.__purificationDoneHRAddress, 1, unit=0x01) #addr, count, slave
        purificationIsDone = int(rr.registers[0])
        self.__slaveLevel.close()
        self.__mutexSlaveLevel.release()

        return not(purificationIsDone)

    def __waterRequest(self):

        self.__mutexSlaveLevel.acquire()
        self.__slaveLevel.connect()
        rr = self.__slaveLevel.read_holding_registers(self.__requestWaterHRAddress, 1, unit=0x01) #addr, count, slave
        waterRequest = int(rr.registers[0])
        self.__slaveLevel.close()
        self.__mutexSlaveLevel.release()

        return not(waterRequest)

    def __setPump(self, state):

        self.__mutexSlavePump.acquire()
        self.__slavePump.connect()

        self.__slavePump.write_register(self.__pumpHRAddress, state, unit=0x01)

        self.__slavePump.close()
        self.__mutexSlavePump.release()

        self.__mutexSQL.acquire()

        query0 = "SELECT COUNT(*) FROM pump"
        self.__cursor.execute(query0)

        pumpCounter = self.__cursor.fetchone()
        if int(pumpCounter[0]) > self.__maxHistoryPump :

			query0 = "DELETE FROM pump WHERE time = (select * from (SELECT MIN(time) FROM pump) t)"
			self.__cursor.execute(query0)

        query0 = "INSERT INTO pump (state) VALUES (%s)" % (str(state))

        self.__cursor.execute(query0)

        self.__mutexSQL.release()

    def __setValve(self, state):

        self.__mutexSlaveLevel.acquire()
        self.__slaveLevel.connect()

        self.__slaveLevel.write_register(self.__valveHRAddress, state, unit=0x01)

        self.__slaveLevel.close()
        self.__mutexSlaveLevel.release()

        self.__mutexSQL.acquire()

        query0 = "SELECT COUNT(*) FROM valve"
        self.__cursor.execute(query0)

        valveCounter = self.__cursor.fetchone()
        if int(valveCounter[0]) > self.__maxHistoryValve :

            query0 = "DELETE FROM valve WHERE time = (select * from (SELECT MIN(time) FROM valve) t)"
            self.__cursor.execute(query0)

        query0 = "INSERT INTO valve (state) VALUES (%s)" % (str(state))

        self.__cursor.execute(query0)

        self.__mutexSQL.release()

    def __insertLevel(self, level):

        self.__mutexSQL.acquire()

        query0 = "SELECT COUNT(*) FROM water_levels"
        self.__cursor.execute(query0)

        levelCounter = self.__cursor.fetchone()
        if int(levelCounter[0]) > self.__maxHistoryLevel :

			query0 = "DELETE FROM water_levels WHERE time = (select * from (SELECT MIN(time) FROM water_levels) t)"
			self.__cursor.execute(query0)

        query0 = "INSERT INTO water_levels (level) VALUES (%s)" % (self.__getLevel())

        self.__cursor.execute(query0)
        self.__mutexSQL.release()
