from master_controller import MasterController
import logging
import sys

if __name__ == "__main__":

	logging.basicConfig()
	log = logging.getLogger()

	if len(sys.argv) < 2:

		print("Default configuration")
		controller =  MasterController("master_conf.ini", "Default")
	else:

		print(sys.argv[1] + " configuration")
		controller =  MasterController("master_conf.ini", sys.argv[1])

	if controller.debugPrints == 1:

		log.setLevel(logging.DEBUG)

	controller.run()
