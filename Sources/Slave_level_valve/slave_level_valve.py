from pymodbus.server.async import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
from twisted.internet.task import LoopingCall
import logging
import RPi.GPIO as GPIO
import sys
import ConfigParser
import time
import threading

GPIO.setmode(GPIO.BCM)
logging.basicConfig()
log = logging.getLogger()

class SlaveLevelValve:

    '''
    The constructor of the class SlaveLevelValve, initializes the class data members
    with the values taken from the configuration file.
    '''
    def __init__(self, confFileName, confName):

        config = ConfigParser.ConfigParser()
        config.read(confFileName)

        self.__valvePin = int(config.get(confName, "valvePin"))
        self.__purificationDonePin = int(config.get(confName, "purificationDonePin"))
        self.__requestWaterPin = int(config.get(confName, "requestWaterPin"))
        self.__triggerPin = int(config.get(confName, "triggerPin"))
        self.__echoPin = int(config.get(confName, "echoPin"))
        self.__IPAddress = config.get(confName, "IPAddress")
        self.__sensorSettingDelay = float(config.get(confName, "sensorSettingDelay"))
        self.__levelUpdateRate = float(config.get(confName, "levelUpdateRate"))
        self.__valveUpdateRate = float(config.get(confName, "valveUpdateRate"))
        self.__levelHRAddress = int(config.get(confName, "levelHRAddress"),16)
        self.__requestWaterHRAddress = int(config.get(confName, "requestWaterHRAddress"),16)
        self.__purificationDoneHRAddress = int(config.get(confName, "purificationDoneHRAddress"),16)
        self.__valveHRAddress = int(config.get(confName, "valveHRAddress"),16)
        self.debugPrints = int(config.get(confName, "debugPrints"))
        self.__vendorName = config.get(confName, "vendorName")
        self.__productCode = config.get(confName, "productCode")
        self.__vendorUrl = config.get(confName, "vendorUrl")
        self.__productName = config.get(confName, "productName")
        self.__modelName = config.get(confName, "modelName")
        self.__majorMinorRevision = config.get(confName, "majorMinorRevision")
        self.__hr_register = 3
        self.__slave_id = 0x00
        self.__lock = threading.RLock()

    '''
    This function initializes the GPIO of the raspberry.
    '''
    def initPins(self):

        GPIO.setwarnings(False)
        GPIO.setup(self.__requestWaterPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.__purificationDonePin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.__valvePin, GPIO.OUT)
        GPIO.setup(self.__triggerPin,GPIO.OUT)
        GPIO.setup(self.__echoPin,GPIO.IN)
        GPIO.output(self.__triggerPin, GPIO.LOW)
        time.sleep(self.__sensorSettingDelay)
        print("GPIO initialization done")

    '''
    This function releases the GPIO resources.
    '''
    def GPIOFree(self):

        GPIO.output(self.__valvePin, GPIO.LOW)
        GPIO.output(self.__triggerPin, GPIO.LOW)
        GPIO.cleanup()

    '''
    This function measures the distance with the ultrasonic sensor
    '''
    def __getDistance(self):

        GPIO.output(self.__triggerPin, True)
        time.sleep(0.00001)
        GPIO.output(self.__triggerPin, False)

        while GPIO.input(self.__echoPin)==0 :

            pulse_start = time.time()

        while GPIO.input(self.__echoPin)==1 :

            pulse_end = time.time()

        pulse_duration = pulse_end - pulse_start
        distance = pulse_duration * 17150
        distance = round(distance, 2)
        #log.debug("Distance:" + str(distance) + "cm")
	return distance

    '''
    This function reads the water level, then it updates the corresponding HR register.
    '''
    def __waterLevelThread(self):

        self.__lock.acquire()
        values = self.__context[self.__slave_id].getValues(self.__hr_register, self.__levelHRAddress, count=1)
	values[0] = self.__getDistance()
        log.debug("WaterLevelThread <--> Water level = " + str(values))
        self.__context[self.__slave_id].setValues(self.__hr_register, self.__levelHRAddress, values)
        self.__lock.release()
    '''
    This function reads the buttons state and updates the pump gpio
    '''
    def __valveButtonsThread(self):

        self.__lock.acquire()
        values = self.__context[self.__slave_id].getValues(self.__hr_register, self.__requestWaterHRAddress, count=3)
        '''
        values[0] = requestWater
        values[1] = purificationDone
        values[2] = valve
        '''

        purificationState = GPIO.input(self.__purificationDonePin)
        requestState = GPIO.input(self.__requestWaterPin)

        if str(values[2]) == '0':

            log.debug("valveButtonsThread <--> Valve off")
            GPIO.output(self.__valvePin, GPIO.LOW)
        elif str(values[1]) == '1':

            log.debug("valveButtonsThread <--> Valve on")
            GPIO.output(self.__valvePin, GPIO.HIGH)

	values[0] = requestState
	values[1] = purificationState
	log.debug("valveButtonThread <-->" + str(values))
        self.__context[self.__slave_id].setValues(self.__hr_register, self.__requestWaterHRAddress, values)
        self.__lock.release()
    '''
    This function starts all needed services.
    '''
    def runServer(self):

        store = ModbusSlaveContext(
    		di=ModbusSequentialDataBlock(0, [17]*100),
    		co=ModbusSequentialDataBlock(0, [17]*100),
    		hr=ModbusSequentialDataBlock(0, [0]*100),
    		ir=ModbusSequentialDataBlock(0, [17]*100))
    	self.__context = ModbusServerContext(slaves=store, single=True)

    	loop_level = LoopingCall(f=self.__waterLevelThread)
        loop_level.start(self.__levelUpdateRate, now=False)
    	loop_valve = LoopingCall(f=self.__valveButtonsThread)
    	loop_valve.start(self.__valveUpdateRate, now=False)

        identity = ModbusDeviceIdentification()
        identity.VendorName = self.__vendorName
        identity.ProductCode = self.__productCode
        identity.VendorUrl = self.__vendorUrl
        identity.ProductName = self.__productName
        identity.ModelName = self.__modelName
        identity.MajorMinorRevision = self.__majorMinorRevision

        print("Starting...")
        StartTcpServer(self.__context, identity=identity, address=(self.__IPAddress, 5020))
