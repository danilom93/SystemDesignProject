from slave_level_valve import SlaveLevelValve
import logging
import sys

if __name__ == "__main__":

	logging.basicConfig()
	log = logging.getLogger()

	if len(sys.argv) < 2:

		print("Default configuration")
		spt = SlaveLevelValve("slave_level_valve_conf.ini", "Default")
	else:

		print(sys.argv[1] + " configuration")
		spt = SlaveLevelValve("slave_level_valve_conf.ini", sys.argv[1])

	if spt.debugPrints == 1:

		log.setLevel(logging.DEBUG)

	spt.initPins()

	try:

		spt.runServer()
	except KeyboardInterrupt:

		spt.GPIOFree()
