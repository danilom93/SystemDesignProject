from slave_pump_temp import SlavePumpTemp
import logging
import sys

if __name__ == "__main__":

	logging.basicConfig()
	log = logging.getLogger()

	if len(sys.argv) < 2:

		print("Default configuration")
		spt = SlavePumpTemp("slave_pump_conf.ini", "Default")
	else:

		print(sys.argv[1] + " configuration")
		spt = SlavePumpTemp("slave_pump_conf.ini", sys.argv[1])

	if spt.debugPrints == 1:

		log.setLevel(logging.DEBUG)

	spt.initPins()

	try:

		spt.runServer()
	except KeyboardInterrupt:

		spt.GPIOFree()
