
from pymodbus.server.async import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
from twisted.internet.task import LoopingCall
import logging
import RPi.GPIO as GPIO
import sys
import Adafruit_DHT
import ConfigParser
import threading

logging.basicConfig()
log = logging.getLogger()

class SlavePumpTemp:

    '''
    The constructor of the class SlavePumpTemp, initializes the class data members
    with the values taken from the configuration file.
    '''
    def __init__(self, confFileName, confName):

        config = ConfigParser.ConfigParser()
        config.read(confFileName)

        self.__pumpPin = int(config.get(confName, "pumpPin"))
        self.__DHTPin = int(config.get(confName, "DHTPin"))
        self.__IPAddress = config.get(confName, "IPAddress")
        self.__temperatureUpdateRate = float(config.get(confName, "temperatureUpdateRate"))
        self.__pumpUpdateRate = float(config.get(confName, "pumpUpdateRate"))
        self.__pumpHRAddress = int(config.get(confName, "pumpHRAddress"),16)
        self.__temperatureHRAddress = int(config.get(confName, "temperatureHRAddress"),16)
        self.__humidityHRAddress = int(config.get(confName, "humidityHRAddress"),16)
        self.debugPrints = int(config.get(confName, "debugPrints"))
        self.__vendorName = config.get(confName, "vendorName")
        self.__productCode = config.get(confName, "productCode")
        self.__vendorUrl = config.get(confName, "vendorUrl")
        self.__productName = config.get(confName, "productName")
        self.__modelName = config.get(confName, "modelName")
        self.__majorMinorRevision = config.get(confName, "majorMinorRevision")
        self.__hr_register = 3
        self.__slave_id = 0x00
        self.__lock = threading.RLock()

    '''
    This function initializes the GPIO of the raspberry.
    '''
    def initPins(self):

	GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.__pumpPin, GPIO.OUT)
        GPIO.output(self.__pumpPin, GPIO.LOW)

    def __pumpOn(self):

        GPIO.output(self.__pumpPin, GPIO.HIGH)

    def __pumpOff(self):

        GPIO.output(self.__pumpPin, GPIO.LOW)

    '''
    This function releases the GPIO resources.
    '''
    def __GPIOFree(self):

        GPIO.output(self.__pumpPin, GPIO.LOW)
        GPIO.cleanup()

    '''
    This function updates the pump state according to the holding register content.
    '''
    def __pumpControllerThread(self):

        self.__lock.acquire()
        values = self.__context[self.__slave_id].getValues(self.__hr_register, self.__pumpHRAddress, count=1)
        log.debug("Pump controller thread <--> Register value = " + str(values))

        if str(values[0]) == '0':

            log.debug("Pump controller thread <--> PUMP OFF")
            self.__pumpOff()
        elif str(values[0]) == '1':

            log.debug("Pump controller thread '1.0'<--> PUMP ON")
            self.__pumpOn()
        self.__lock.release()
    '''
    This function reads the temperature and the humidity, then it updates the
    corresponding holding register content.
    '''
    def __temperatureHumidityThread(self):

        self.__lock.acquire()
        values = self.__context[self.__slave_id].getValues(self.__hr_register, self.__temperatureHRAddress, count=2)
        humidity, temperature = Adafruit_DHT.read_retry(11, self.__DHTPin)
        values[0] = temperature
        values[1] = humidity
        log.debug("Temperature humidity thread <--> New values: " + str(values))
        self.__context[self.__slave_id].setValues(self.__hr_register, self.__temperatureHRAddress, values)
        self.__lock.release()
    '''
    This function starts all needed services.
    '''
    def runServer(self):

        store = ModbusSlaveContext(
    		di=ModbusSequentialDataBlock(0, [17]*100),
    		co=ModbusSequentialDataBlock(0, [17]*100),
    		hr=ModbusSequentialDataBlock(0, [0]*100),
    		ir=ModbusSequentialDataBlock(0, [17]*100))
    	self.__context = ModbusServerContext(slaves=store, single=True)

    	loop_pump = LoopingCall(f=self.__pumpControllerThread)
	loop_pump.start(self.__pumpUpdateRate, now=False)

    	loop_temp = LoopingCall(f=self.__temperatureHumidityThread)
    	loop_temp.start(self.__temperatureUpdateRate, now=False)

        identity = ModbusDeviceIdentification()
        identity.VendorName = self.__vendorName
        identity.ProductCode = self.__productCode
        identity.VendorUrl = self.__vendorUrl
        identity.ProductName = self.__productName
        identity.ModelName = self.__modelName
        identity.MajorMinorRevision = self.__majorMinorRevision

	print("Starting...")
        StartTcpServer(self.__context, identity=identity, address=(self.__IPAddress, 5020))
