<?php

	include '../db_connect.php';
    include '../functions.php';
    include '../db_connect_values.php';

	// Inserisci in questo punto il codice per la connessione al DB e l'utilizzo delle varie funzioni.
	sec_session_start();
	if(login_check($mysqli) == true) {
		//query to get data from the table
		$query = sprintf("SELECT time, level FROM water_levels order by time desc limit 50");

		//execute query
		$result = $mysqliValues->query($query);

		//loop through the returned data
		$data = array();
		foreach ($result as $row) {
		  $data[] = $row;
		}

		$dataReversed = array_reverse($data);

		//free memory associated with result
		$result->close();

		//now print the data
		print json_encode($dataReversed);
	}
	else 
	{
    	echo 'You are not authorized to access this page, please login. <br/>';
    }

//setting header to json
header('Content-Type: application/json');