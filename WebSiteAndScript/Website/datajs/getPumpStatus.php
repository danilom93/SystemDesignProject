<?php

	include '../db_connect.php';
    include '../functions.php';
    include '../db_connect_values.php';

	// Inserisci in questo punto il codice per la connessione al DB e l'utilizzo delle varie funzioni.
	sec_session_start();
	if(login_check($mysqli) == true) {
		//query to get data from the table
		$query = sprintf("SELECT state FROM pump WHERE time = (SELECT max(time) from pump)");

		$result = mysqli_query($mysqliValues, $query);
        $row = mysqli_fetch_array($result);
        $pumpValue = $row['state'];

		//now print the data
		print json_encode($pumpValue);
	}
	else 
	{
    	echo 'You are not authorized to access this page, please login. <br/>';
    }

//setting header to json
header('Content-Type: application/json');