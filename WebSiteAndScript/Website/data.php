<!DOCTYPE HTML>  

<?php
    //force https
    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
      header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], TRUE, 301);
      die();
    }

    // $page = $_SERVER['PHP_SELF'];
    // $sec = "15";
?>

<html>

<head>
    <style>
        .error {color: #FF0000;}

        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto
        }
    </style>
	<link rel="stylesheet" href="css/industrial.css">
    <link rel="stylesheet" href="css/introjs.min.css">
    <link rel="stylesheet" href="css/foundation.css">
    <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">

</head>

<body>  

    <?php

    	include 'db_connect.php';
        include 'functions.php';

    	// Inserisci in questo punto il codice per la connessione al DB e l'utilizzo delle varie funzioni.
    	sec_session_start();
    	if(login_check($mysqli) == true) {

    		$role = $_SESSION['role'];

    		if($role == 1)
    		{
                echo $_SESSION['username'] . str_repeat('&nbsp;', 10);                 
                echo '<input type="button" value="Logout" onclick="document.location.href=\'logout.php\'" /><br><br>';

                echo '<div style="float:left; display:block;">';
                    echo "<h1>Valve</h1>Status: <span id=\"statusValve\"></span>" . "<br><br>";
                echo '</div>';

                echo '<div style="float:left; display:block; padding-left: 50px;">';
                    echo "<h1>Pump</h1>Status: <span id=\"statusPump\"></span>" . "<br><br>";
                echo '</div>';

                echo '<div style="clear:both;">';
                echo '</div>'; 

                echo "<h1>Temperature - Humidity</h1>Temperature: " . "<span id=\"temperature\"></span>" . " °C<br>Humidity: " . "<span id=\"humidity\"></span>" . " %<br><br>";
                echo "<h1>Water level</h1>Level: <span id=\"waterLevel\"></span>". " cm <br><br>";
    		}
    		else
    		{
    			echo $_SESSION['username'] . str_repeat('&nbsp;', 10);
                echo '<input type="button" value="Logout" onclick="document.location.href=\'logout.php\'" /><br><br>';

            	echo '<div style="float:left; display:block;">';
                    echo "<h1>Valve</h1>Status: <span id=\"statusValve\"></span>" . "<br><br>";
                    echo '<input type="button" value="Download Valve csv" onclick="document.location.href=\'downloadValve.php\'" /><br><br>';
                echo '</div>';

    			echo '<div style="float:left; display:block; padding-left: 50px;">';
                    echo "<h1>Pump</h1>Status: <span id=\"statusPump\"></span>" . "<br><br>";
                    echo '<input type="button" value="Download Valve csv" onclick="document.location.href=\'downloadValve.php\'" /><br><br>';
                echo '</div>';

				echo '<div style="float:left; display:block; padding-left: 50px;">';
                    echo '<h2>Water Level</h2>
                        <div class="industrial tank size two" >
                            <span class="ticks" data-amount="4" data-scale-freq="1"></span>
                            <div class="space"></div>
                        	<div class="meter"></div>
                        </div>';
                echo '</div>';

                echo '<div style="clear:both;">';
                echo '</div>'; 


                echo '<br><br><div style="float:left; display:block;">';
                    echo "<h1>Temperature - Humidity</h1>Temperature: " . "<span id=\"temperature\"></span>" . " °C<br>Humidity: " . "<span id=\"humidity\"></span>" . " %<br><br>";
                    echo '<input type="button" value="Download Temperature-Humidity csv" onclick="document.location.href=\'downloadTempHum.php\'" /><br><br>';
                    echo '<input type="button" value="Zoom Chart" onclick="document.location.href=\'lineGraphTempHum.html\'" />';
                echo '</div>';
                echo '<div class="chart-container" style="float:left; display:block; padding-left: 50px; height: 300px; width:500px;">';
                    echo '<canvas id="canvasTemp"></canvas>';
                echo '</div>';
                echo '<div class="chart-container" style="float:left; display:block; padding-left: 50px; height: 300px; width:500px;">';
                    echo '<canvas id="canvasHum"></canvas>';
                echo '</div>';
                echo '<div style="clear:both;">';
                echo '</div>'; 

                echo '<div style="float:left; display:block;">';
                    echo "<h1>Water level</h1>Level: <span id=\"waterLevel\"></span>". " cm <br><br>";
                    echo '<input type="button" value="Download Water Level csv" onclick="document.location.href=\'downloadWaterLevel.php\'" /><br><br>';
                    echo '<input type="button" value="Zoom Chart" onclick="document.location.href=\'lineGraphWaterLevel.html\'" />';
                echo '</div>';
                echo '<div class="chart-container" style="height: 300px; width:500px; float:left; display:block; padding-left: 50px;">';
                    echo '<br><br><canvas id="canvasWaterLevel"></canvas>';
                echo '</div>';
                echo '<div style="clear:both;">';
                echo '</div>';
        }
            
    	
    	   // Inserisci qui il contenuto delle tue pagine!
    	 
    	} else {
    	   echo 'You are not authorized to access this page, please login. <br><br>';
           echo '<input type="button" value="Login" onclick="document.location.href=\'login.php\'" />';
    	}
    ?>

    <!-- <div class="chart-container" style="height: 500px; width:500px;">
      <canvas id="canvasWaterLevel"></canvas>
    </div> -->
    
    <!-- javascript -->
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/industrial.js"></script>
    <script src="js/app.js"></script>
    <script src="js/intro.min.js"></script>
    <script type="text/javascript" src="./chart/Chart.min.js"></script>
    <script type="text/javascript" src="./chart/lineGraphWaterLevel.js"></script>
    <script type="text/javascript" src="./chart/lineGraphTemperature.js"></script>
    <script type="text/javascript" src="./chart/lineGraphHumidity.js"></script>
    <script type="text/javascript" src="./statusValve.js"></script>
    <script type="text/javascript" src="./statusPump.js"></script>
    <script type="text/javascript" src="./waterLevel.js"></script>
    <script type="text/javascript" src="./tempHum.js"></script>

    


</body>

</html>
