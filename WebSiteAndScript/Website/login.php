<script type="text/javascript" src="sha512.js"></script>
<script type="text/javascript" src="forms.js"></script>

<?php

	//force https
    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
      header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], TRUE, 301);
      die();
    }

	if(isset($_GET['error'])) { 
	   echo "<span style=\"color:red; font-weight: bold\">Error Logging In!</span>";
	}

	

	// $password = "w2s0s1E8VA_employee1";
	// $random_salt = "e39a8ed317cb43fdc78bcd7f574c72d76ac5c2affaa3a5e648bb999f1f647f4c4279130d0c1373ca7ecf6c232add6f1528022dbce2d96d4b537637b241bbcd28";

	// echo "<br><br>";
	// echo $random_salt;
	// echo "<br><br>";
	// echo $random_salt.$password.$random_salt;	
	// echo "<br><br>";
	// echo $password;	
	// echo "<br><br>";
	// echo $password_hash;
	// echo "<br><br>";

	// if(isset($_POST['email'], $_POST['p']))
	// {
	// 	$email = $_POST['email'];
 //    	$password = $_POST['p'];
 //    	echo $password;	
	// 	echo "<br><br>";
	// 	echo "<br><br>";
	// 	echo "<br><br>";
	// 	$password_hash = hash('sha512', $random_salt.$password.$random_salt);
	// 	echo $password_hash;
	// 	echo "<br><br>";
	// }

	// echo "<br><br>";
?>

<!DOCTYPE html>
<html>
  <title>Login</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
<body>

	<div class="log_in">
	  <form action="process_login.php" method="post" name="login_form">
	    <fieldset>
	      <legend><h3>LOG IN</h3></legend>
	      <label>Email</label>
	      <br>
	      <input type="text" name="email" placeholder="Email" required oninvalid="this.setCustomValidity('Insert email')" class="fieldform" autocomplete="on">
	      <br><br>
	      <label>Password</label>
	      <br>
	      <input type="password" name="p" placeholder="Password" required oninvalid="this.setCustomValidity('Wrong password')"  class="fieldform" id="password">
	      <br><br>
	        <input type="button" value="Login" onclick="formhash(this.form, this.form.password);" id="buttonId"/>
	      <br><br>
	    </fieldset>
	  </form>
	</div>

</body>

<script type="text/javascript">
	
	var input = document.getElementById("password");
	input.addEventListener("keyup",
		function(event){	
			event.preventDefault();
			if(event.keyCode===13)
			{
				document.getElementById("buttonId").click();
			}
		});

</script>