<?php

	include 'db_connect.php';
  	include 'functions.php';
  	include 'db_connect_values.php';

  	sec_session_start();
	if(login_check($mysqli) == true) {
		$result = $mysqliValues->query('SELECT * FROM pump');
	    if (!$result) die('Couldn\'t fetch records');
	    $num_fields = mysqli_num_fields($result);
	    $headers = array();
	    while ($fieldinfo = mysqli_fetch_field($result)) {
	        $headers[] = $fieldinfo->name;
	    }
	    $fp = fopen('php://output', 'w');
	    if ($fp && $result) {
	        header('Content-Type: text/csv');
	        header('Content-Disposition: attachment; filename="pump.csv"');
	        header('Pragma: no-cache');
	        header('Expires: 0');
	        fputcsv($fp, $headers);
	        while ($row = $result->fetch_array(MYSQLI_NUM)) {
	            fputcsv($fp, array_values($row));
	        }
	        die;
	    }

	    header('Location: ./data.php');
        exit();
	}
	else {
	   echo 'You are not authorized to access this page, please login. <br/>';
	}

  	


?>