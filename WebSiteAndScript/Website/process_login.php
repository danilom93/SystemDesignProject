<?php

   //force https
    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
      header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], TRUE, 301);
      die();
    }

   include 'db_connect.php';
   include 'functions.php';
   sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
   if(isset($_POST['email'], $_POST['p'])) { 
      $email = $_POST['email'];
      $password = $_POST['p']; // Recupero la password criptata.
      if(login($email, $password, $mysqli) == true) {
         // Login eseguito
         echo 'Success: You have been logged in!';
         header('Location: ./data.php');
         exit();
      } else {
         // Login fallito
         header('Location: ./login.php?error=1');
         exit();
      }
   } else { 
      // Le variabili corrette non sono state inviate a questa pagina dal metodo POST.
      echo 'Invalid Request';
   }

?>