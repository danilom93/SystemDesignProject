$(document).ready(function(){
  setInterval(function(){
    $.ajax({
      url : "https://192.168.0.203/datajs/getAllWaterLevels.php",
      type : "GET",
      success : function(data){
        console.log(data);

        var time = [];
        var level = [];

        for(var i in data) {
          time.push(data[i].time);
          level.push(data[i].level);
        } 

        var chartdata = {
          labels: time,
          datasets: [
            {
              label: "Water Level",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(59, 89, 152, 0.75)",
              borderColor: "rgba(59, 89, 152, 1)",
              pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
              pointHoverBorderColor: "rgba(59, 89, 152, 1)",
              data: level
            }
          ],
          options: {
              animation: false
          }
        };

        var ctx = $("#canvasWaterLevel");

        var LineGraph = new Chart(ctx, {
          type: 'line',
          data: chartdata
        });
      },
      error : function(data) {

      }
    });
  }, 15000);

  $.ajax({
      url : "https://192.168.0.203/datajs/getAllWaterLevels.php",
      type : "GET",
      success : function(data){
        console.log(data);

        var time = [];
        var level = [];

        for(var i in data) {
          time.push(data[i].time);
          level.push(data[i].level);
        } 

        var chartdata = {
          labels: time,
          datasets: [
            {
              label: "Water Level",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(59, 89, 152, 0.75)",
              borderColor: "rgba(59, 89, 152, 1)",
              pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
              pointHoverBorderColor: "rgba(59, 89, 152, 1)",
              data: level
            }
          ],
          options: {
              animation: false
          }
        };

        var ctx = $("#canvasWaterLevel");

        var LineGraph = new Chart(ctx, {
          type: 'line',
          data: chartdata
        });
      },
      error : function(data) {

      }
    });
});
