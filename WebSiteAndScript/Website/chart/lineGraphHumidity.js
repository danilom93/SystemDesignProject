$(document).ready(function(){
  setInterval(function(){
    $.ajax({
      url : "https://192.168.0.203/datajs/getAllTempHum.php",
      type : "GET",
      success : function(data){
        console.log(data);

        var time = [];
        var hum = [];

        for(var i in data) {
          time.push(data[i].time);
          hum.push(data[i].humidity);
        } 

        var chartdata = {
          labels: time,
          datasets: [
            {
              label: "Humidity",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0, 255, 0, 0.75)",
              borderColor: "rgba(0, 255, 0, 1)",
              pointHoverBackgroundColor: "rgba(0, 255, 0, 1)",
              pointHoverBorderColor: "rgba(0, 255, 0, 1)",
              data: hum
            }
          ]
        };

        var ctx = $("#canvasHum");

        var LineGraph = new Chart(ctx, {
          type: 'line',
          data: chartdata
        });
      },
      error : function(data) {

      }
    });
  }, 15000);

  $.ajax({
      url : "https://192.168.0.203/datajs/getAllTempHum.php",
      type : "GET",
      success : function(data){
        console.log(data);

        var time = [];
        var hum = [];

        for(var i in data) {
          time.push(data[i].time);
          hum.push(data[i].humidity);
        } 

        var chartdata = {
          labels: time,
          datasets: [
            {
              label: "Humidity",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0, 255, 0, 0.75)",
              borderColor: "rgba(0, 255, 0, 1)",
              pointHoverBackgroundColor: "rgba(0, 255, 0, 1)",
              pointHoverBorderColor: "rgba(0, 255, 0, 1)",
              data: hum
            }
          ]
        };

        var ctx = $("#canvasHum");

        var LineGraph = new Chart(ctx, {
          type: 'line',
          data: chartdata
        });
      },
      error : function(data) {

      }
    });
});
