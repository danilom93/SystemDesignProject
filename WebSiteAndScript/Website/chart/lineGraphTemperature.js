$(document).ready(function(){
  setInterval(function(){
    $.ajax({
      url : "https://192.168.0.203/datajs/getAllTempHum.php",
      type : "GET",
      success : function(data){
        console.log(data);

        var time = [];
        var temp = [];

        for(var i in data) {
          time.push(data[i].time);
          temp.push(data[i].temperature);
        } 

        var chartdata = {
          labels: time,
          datasets: [
            {
              label: "Temperature",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(255, 0, 0, 0.75)",
              borderColor: "rgba(255, 0, 0, 1)",
              pointHoverBackgroundColor: "rgba(255, 0, 0, 1)",
              pointHoverBorderColor: "rgba(255, 0, 0, 1)",
              data: temp
            }
          ]
        };

        var ctx = $("#canvasTemp");

        var LineGraph = new Chart(ctx, {
          type: 'line',
          data: chartdata
        });
      },
      error : function(data) {

      }
    });
  }, 15000);

  $.ajax({
      url : "https://192.168.0.203/datajs/getAllTempHum.php",
      type : "GET",
      success : function(data){
        console.log(data);

        var time = [];
        var temp = [];

        for(var i in data) {
          time.push(data[i].time);
          temp.push(data[i].temperature);
        } 

        var chartdata = {
          labels: time,
          datasets: [
            {
              label: "Temperature",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(255, 0, 0, 0.75)",
              borderColor: "rgba(255, 0, 0, 1)",
              pointHoverBackgroundColor: "rgba(255, 0, 0, 1)",
              pointHoverBorderColor: "rgba(255, 0, 0, 1)",
              data: temp
            }
          ]
        };

        var ctx = $("#canvasTemp");

        var LineGraph = new Chart(ctx, {
          type: 'line',
          data: chartdata
        });
      },
      error : function(data) {

      }
    });
});
