
DROP DATABASE IF EXISTS valuesSensorsActuators;
CREATE DATABASE valuesSensorsActuators;

CREATE USER 'std_user' IDENTIFIED BY 'eKcKZr65zAa8BEPU';
GRANT SELECT ON `valuesSensorsActuators`.* TO 'std_user';

USE valuesSensorsActuators;

CREATE TABLE temp_hum ( temperature int,
                        humidity int,
                        time timestamp(4) key);

CREATE TABLE water_levels ( level int,
                            time timestamp(4) key);

CREATE TABLE pump ( state int,
                    time timestamp(4) key);

CREATE TABLE valve (  state int,
                      time timestamp(4) key);

INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);
INSERT INTO temp_hum (temperature, humidity) VALUES (33, 752);

INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);
INSERT INTO water_levels (level) VALUES (69);

INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);
INSERT INTO pump (state) VALUES (0);

INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);
INSERT INTO valve (state) VALUES (0);


