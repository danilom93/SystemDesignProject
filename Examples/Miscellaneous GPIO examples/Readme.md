# Miscellaneous examples
This folder contains some basic examples on how to use Rasperry pi gpio with python.

## led_blink.py
This example just allows a led to blink with a given frequency.

### Hardware configuration
The required materials for running this example are:
- A working Rasperry pi
- One led
- One 560Ω resistor
- Two wires female to male
- One breadboard

How to connect the wires?

![](images/led_blink.jpg)

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python led_blink.py
```

## ultrasonic_sensor.py
This example shows how to use an HC-SR04 ultrasonic range sensor on the Raspberry Pi.

### Hardware configuration
The required materials for running this example are:
- A working Rasperry pi
- HC-SR04
- One 1kΩ Resistor
- one 2kΩ Resistor
- Jumper Wires
- One breadboard

How to connect the wires?

![](images/ultrasonic_sensor.png)

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python ultrasonic_sensor.py
```

## temperature_s$ python ultrasonic_sensor.pyensor.py
This example shows how to use a DHT-11 temperature and humidity sensor on the Raspberry Pi.

### Hardware configuration
The required materials for running this example are:
- A working Rasperry pi
- DHT-11
- One 10kΩ Resistor
- Jumper Wires
- One breadboard

How to connect the wires?
$ python ultrasonic_sensor.py
![](images/temperature_sensor.png)

### How to run the program
To run the example some preliminary steps have to be done:

- Open a shell and type:

```
$ cd ~
$ git clone https://github.com/adafruit/Adafruit_Python_DHT.git
$ cd Adafruit_Python_DHT
$ sudo python setup.py install
```

- Once the library has been installed, go to the Miscellaneous example folder and type:

```
$ python temperature_sensor.py
```
