# Pymodbus examples
This folder contains a basic example that allows the communication between a server and a client which exchange data through modbus over TCP. More in details:
1. The client writes periodically one slave holding register.
2. The server(slave) checks periodically the content of the previous register and controls a led accordingly.

## server.py
This program should be run by the actuator/sensor machine (slave) that receives commands by the master.

### Hardware configuration
The required materials for running this example are:
- A working Rasperry pi
- One led
- One 560Ω resistor
- Two wires female to male
- One breadboard

How to connect the wires?

![](../Miscellaneous GPIO examples/images/led_blink.jpg)

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python server.py
```

## client.py
This program should be run by the controller machine (master) that sends commands to the slave.

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python client.py
```
Note: The client has to be run only when the server is already waiting for connections.
