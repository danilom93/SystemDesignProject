#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
--------------------------------------------------------------------------

This is an example of having a background thread updating the
context while the server is operating. This can also be done with
a python thread::

    from threading import Thread

    thread = Thread(target=updating_writer, args=(context,))
    thread.start()
"""
# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.server.async import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer

import RPi.GPIO as GPIO

LedPin = 11    #defines the led (pin11)

def setup():

  GPIO.setmode(GPIO.BOARD)          # Numbers GPIOs by physical location
  GPIO.setup(LedPin, GPIO.OUT)      # Set LedPin's mode is output
  GPIO.output(LedPin, GPIO.HIGH)    # Set LedPin high(+3.3V) to turn on led

def led_on():
    GPIO.output(LedPin, GPIO.HIGH)  # led on

def led_off():
    GPIO.output(LedPin, GPIO.LOW)   # led off

def destroy():
  log.debug("Gpio free")
  GPIO.output(LedPin, GPIO.LOW)     # led off
  GPIO.cleanup()                    # Release resource

# --------------------------------------------------------------------------- #
# import the twisted libraries we need
# --------------------------------------------------------------------------- #
from twisted.internet.task import LoopingCall

# --------------------------------------------------------------------------- #
# configure the service logging
# --------------------------------------------------------------------------- #
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)

# --------------------------------------------------------------------------- #
# define your callback process
# --------------------------------------------------------------------------- #


def updating_writer(a):
    """ A worker process that runs every so often and
    updates live values of the context. It should be noted
    that there is a race condition for the update.

    :param arguments: The input arguments to the call
    """

    log.debug("Updating thread called")
    #retrieves the arguments passed to thread
    context = a[0]
    register = 3
    slave_id = 0x00
    address = 0x10

    values = context[slave_id].getValues(register, 3, count=1)
    log.debug("Register value = " + str(values))

    if str(values[0]) == '0':
	
        log.debug("LED OFF")
    	led_off()
    elif str(values[0]) == '1':

    	log.debug("LED ON")
        led_on()
    
def run_updating_server():
    # ----------------------------------------------------------------------- # 
    # initialize your data store
    # ----------------------------------------------------------------------- # 
    
    store = ModbusSlaveContext(
        di=ModbusSequentialDataBlock(0, [17]*100),
        co=ModbusSequentialDataBlock(0, [17]*100),
        hr=ModbusSequentialDataBlock(0, [0]*100),
        ir=ModbusSequentialDataBlock(0, [17]*100))
    context = ModbusServerContext(slaves=store, single=True)
    
    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/bashwork/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'
    identity.MajorMinorRevision = '1.0'
    
    # ----------------------------------------------------------------------- # 
    # run the server you want
    # ----------------------------------------------------------------------- # 
    time = 0.1  # 100 milliseconds delay
    loop = LoopingCall(f=updating_writer, a=(context,))
    loop.start(time, now=False) # initially delay by time
    StartTcpServer(context, identity=identity, address=("192.168.1.201", 5020))


if __name__ == "__main__":
	setup()
	try:
		run_updating_server()
	except KeyboardInterrupt:
		destroy()
