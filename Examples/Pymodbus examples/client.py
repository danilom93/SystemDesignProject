from pymodbus.client.sync import ModbusTcpClient
from pprint import pprint
import time

#creates a connection with the slave having the specified IP address
client = ModbusTcpClient('192.168.1.201', port=5020)
client.connect()

#loops to sequentially send commands
while(1):

    #writes 1 to holding register number 3
	rq = client.write_register(3, 1, unit=0x01) 
    #waits for 500 milliseconds
	time.sleep(0.5)
    #writes 0 to holding register number 3
	rq = client.write_register(3, 0, unit=0x01)
    #waits for 500 milliseconds
	time.sleep(0.5)

client.close()

