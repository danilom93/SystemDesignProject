# Controller example
In this example, 3 raspberry pi are used. One raspberry pi acts as a controller, while other two act as slaves.
The master reads from one slave the distance measured by the ultrasonic sensor and based on this distance the master asks the other slave to toggle its LED accordingly.

## slave_distance.py
This program should be run by the machine (slave) with the ultrasonic sensor attached. More in details,
it measures and stores periodically the distance in a register.   

### Hardware configuration
The required materials for running this example are:
- A working Rasperry pi
- HC-SR04
- One 1kΩ Resistor
- one 2kΩ Resistor
- Jumper Wires
- One breadboard

How to connect the wires?

![](../Miscellaneous GPIO examples/images/ultrasonic_sensor.png)

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python slave_distance.py
```

## slave_led.py
This program should be run by the machine (slave) with the led attached. More in details,
it turns on/off the LED based on the controller command.  

### Hardware configuration
The required materials for running this example are:
- A working Rasperry pi
- One led
- One 560Ω resistor
- Two wires female to male
- One breadboard

How to connect the wires?

![](../Miscellaneous GPIO examples/images/led_blink.jpg)

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python slave_led.py
```

## master.py  
This program should be run by the controller machine (master) that:
- checks periodically the distance measured by the slave_distance (server)
- asks to slave_led to turn on a led when distance is less than a given threshold

### How to run the program
To run the example, open a shell, move into the examples folder and type:

```
$ python master.py
```

Note: master.py has to be executed only when slave_led and slave_distance are already running.
